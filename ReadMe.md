# Andar Robot #

Andar is a robot, built with:

* An Arduino Mega ADK as **Backbone**
* An Android phone as **Brain**
* An Android phone as **Remote** control.

This is an open source project, if you would like to take part on it, please be my guest, I would love to see your changes and improvements on this code!

Details on the project can be found at [http://letsmakerobots.com/robot/project/andar](http://letsmakerobots.com/robot/project/andar)

![At6_q0dv0-2kpVNIfu1iw3UFAAsX7kDWX_BEQptg3Two.jpg](http://letsmakerobots.com/files/field_primary_image/20151014_204809.jpg)