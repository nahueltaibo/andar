#include "AndroidConnection.h"

AndroidConnection::AndroidConnection() : AndroidAccessory("NahuelTaibo", "Andar", "A robot based on Arduino and Android", "1.0", "http://letsmakerobots.com/robot/project/andar", "1")
{
}


AndroidConnection::~AndroidConnection()
{
}

void AndroidConnection::writeMessage(Message message)
{
	int length = message.getTotalLength();
	byte* bytes = message.getBytes();

	message.debug(false);

	noInterrupts();  // disable interrupts
	write(bytes, length);
	interrupts();  // enable interrupts
}

Message AndroidConnection::readMessage()
{
	byte length = 0;
	byte value;
	Message message(NO_MESSAGE);

	if (isConnected()) {
		if (available() > 0) {

			//Read the type of the message
			byte type = read();

			//Read the length of the message
			while (available() < 1) {};
			byte payloadLength = read();

			byte bufferLen = HEADER_LENGTH + payloadLength;
			byte buffer[bufferLen];

			buffer[0] = type;
			buffer[1] = payloadLength;
			
			//Read the payload of the message
			while (available() < payloadLength) {};

			for (int i = 0; i < payloadLength; i++)
			{
				value = read();

				buffer[i + HEADER_LENGTH] = value;
			}

			Message message(buffer, bufferLen);
		
			message.debug(true);

			return message;
		}
	}

	return message;
}

void AndroidConnection::processPing(Message ping)
{
	Message pong(PONG);

	while (ping.payloadAvailable())
	{
		pong.push((byte)(ping.popByte() + 1));
	}

	writeMessage(pong);
}

void AndroidConnection::processHealthSecuence(Message healthSequence)
{
	//Receive PUBLISH without payload
	Message message(NO_MESSAGE);

	for (message = readMessage(); message.getType() == NO_MESSAGE; message = readMessage());

	if (message.getType() != PUBLISH)
	{
		Serial.print("Health Error: expecting PUBLISH, received ");
		Serial.println(message.getType());
	}
	else
	{
		if (message.payloadAvailable())
		{
			Serial.print("Health Error: expecting null payload, bytes received ");
			Serial.println(message.getLength());
		}
		else
		{
			writeMessage(message);
		}
	}

	//Receive PUBLISH with 3 int payload
	for (message = readMessage(); message.getType() == NO_MESSAGE; message = readMessage());

	if (message.getType() != PUBLISH)
	{
		Serial.print("Health Error: expecting PUBLISH, received ");
		Serial.println(message.getType());
	}
	else
	{
		if (message.getLength() != 3 * sizeof(int))
		{
			Serial.print("Health Error: expecting 3 int payload, bytes received ");
			Serial.println(message.getLength());
		}
		else{
			Serial.print("intA: ");
			Serial.println(message.popInt());

			Serial.print("intB: ");
			Serial.println(message.popInt());

			Serial.print("intC: ");
			Serial.println(message.popInt());

			writeMessage(message);
		}
	}

	//PUBLISH with MAX payload ints
	for (message = readMessage(); message.getType() == NO_MESSAGE; message = readMessage());
	if (message.getType() != PUBLISH)
	{
		Serial.print("Health Error: expecting PUBLISH, received ");
		Serial.println(message.getType());
	}
	else
	{
		if (message.getLength() != (MAX_MESSAGE_LENGTH - HEADER_LENGTH))
		{
			Serial.print("Health Error: expecting full payload, bytes received ");
			Serial.println(message.getLength());
		}
		else{
			writeMessage(message);
		}
	}
}
