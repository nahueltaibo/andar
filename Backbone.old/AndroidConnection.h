#ifndef AndroidConnection_h
#define AndroidAccessory_h


#include "AndroidAccessory.h"
#include "Message.h"

class AndroidConnection : public AndroidAccessory
{
public:
	AndroidConnection();
	~AndroidConnection();
	
	void writeMessage(Message message);
	Message readMessage();

	void processHealthSecuence(Message healthSequence);
	void processPing(Message ping);
};

#endif