#ifndef Backbone_h
#define Backbone_h

#define DEBUG

//PINOUT DEFINITION
//Motors
#define PIN_ENB 5
#define PIN_IN4 8 //6
#define PIN_IN3 9 //7
#define PIN_IN2 6 //8
#define PIN_IN1 7 //9
#define PIN_ENA 10

#define PIN_LED 13

//The messages recognized by Andar Robot
enum AndarMessages {
	MOTORS,
};

#endif
