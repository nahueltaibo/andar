#include "MotorsMessage.h"


MotorsMessage::MotorsMessage(Message base) : Message(base.getType())
{
	type = (AndarMessages)popByte();
	acceleration = base.popByte();
	leftSpeed = base.popInt();
	rightSpeed = base.popInt();
}


MotorsMessage::~MotorsMessage()
{
}

AndarMessages MotorsMessage::getType()
{
	return type;
}

byte MotorsMessage::getAcceleration()
{
	return acceleration;
}

int MotorsMessage::getLeftSpeed()
{
	return leftSpeed;
}

int MotorsMessage::getRightSpeed()
{
	return rightSpeed;
}

