#include "AndarMotors.h"

AndarMotors::AndarMotors() : L298NMotors(PIN_ENA, PIN_ENB, PIN_IN1, PIN_IN2, PIN_IN3, PIN_IN4)
{
}


AndarMotors::~AndarMotors()
{
}

void AndarMotors::processMotors(MotorsMessage motorsMessage)
{
	Serial.print("Motors: Acc: ");
	Serial.print(motorsMessage.getAcceleration());
	Serial.print(" Left: ");
	Serial.print(motorsMessage.getLeftSpeed());
	Serial.print(" Right: ");
	Serial.println(motorsMessage.getRightSpeed());

	move(motorsMessage.getLeftSpeed(), motorsMessage.getRightSpeed());
}
