#include <UsbHost.h>
#include <Max3421e_constants.h>
#include <Max3421e.h>
#include <Wire.h>
#include <ch9.h>
#include <AndroidAccessory.h>
#include "Backbone.h"
#include "Message.h"
#include "AndroidConnection.h"
#include "L298NMotors.h"
#include "MotorsMessage.h"
#include "AndarMotors.h"
#include "Neck.h"

boolean debug = true;

// Initialize the accessory.
AndroidConnection connection;
AndarMotors motors;

// timer
long timer = millis();

void processPublish(Message message){
	//motors.move(leftMotor, rightMottor);

	switch (message.popByte()){
	case MOTORS:
		MotorsMessage motorsMessage(message);
		motors.processMotors(motorsMessage);
		break;
	}
}

void setup() {
	
	// use the serial port to monitor that things work
	Serial.begin(9600);
	Serial.println("ready");


  // Configure the connection with Neck
  Neck.setup();
  
	// join i2c bus (address optional for master)
	Wire.begin();

	// initiate the communication to the phone
	connection.model = "Andar";
	connection.begin();
}

void loop() {

	Message message = connection.readMessage();
	if (message.getType() != NO_MESSAGE)
	{
		// depending on the package type do different things
		switch (message.getType()) {
		case  PING:
			//Payload is a byte with a random number. we should reply with the following number
			connection.processPing(message);
			break;
		case  HEALTH_SECUENCE:
			//Payload is a byte with a random number. we should reply with the following number
			connection.processHealthSecuence(message);
			break;
		case  PUBLISH:
			//Payload is a byte with a random number. we should reply with the following number
			processPublish(message);
			break;
		case  DISCONNECT:
			motors.move(0, 0);
			break;
		default:
			break;
		}
	}

  // Configure the connection with Neck
  Neck.update();

	if (!connection.isConnected()) {

		// Stop everything
		motors.move(0, 0);

		delay(500);

		Serial.println("No connection with Brain. Reconnecting...");

		//try to reconnect
		connection.begin();
	}
}
