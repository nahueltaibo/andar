#include "L298NMotors.h"


L298NMotors::L298NMotors(int enA, int enB, int in1, int in2, int in3, int in4)
{
	// motor one
	enableA = enA;
	input1 = in1;
	input2 = in2;
	
	// motor two
	enableB = enB;
	input3 = in3;
	input4 = in4;

	// set all the motor control pins to outputs
	pinMode(enableA, OUTPUT);
	pinMode(enableB, OUTPUT);
	pinMode(input1, OUTPUT);
	pinMode(input2, OUTPUT);
	pinMode(input3, OUTPUT);
	pinMode(input4, OUTPUT);
}


L298NMotors::~L298NMotors()
{
	//TODO: stop the motors

}

void L298NMotors::printSpeeds(int left, int right)
{
	Serial.print("left: ");
	Serial.print(left);
	Serial.print(" right: ");
	Serial.println(right);
}

void L298NMotors::move(int leftSpeed, int rightSpeed)
{
	//Check leftSpeed range
	if (leftSpeed < -100 || leftSpeed>100)
	{
		Serial.write("ERROR: leftSpeed not between -100 and 100");
		//STOP the motors
		leftSpeed = rightSpeed = 0;
	}
	
	//Check rightSpeed range
	if (rightSpeed < -100 || rightSpeed >100)
	{
		Serial.write("ERROR: rightSpeed not between -100 and 100");
		//STOP the motors
		leftSpeed = rightSpeed = 0;
	}

	int realLSpeed, realRSpeed;

	// -100 is max backward speed
	// 0 is stop
	// 100 is max forward speed

	//Transform it to motor values (0 - 255)
	realLSpeed = leftSpeed * 255 / 100;
	realRSpeed = rightSpeed * 255 / 100;

	printSpeeds(realLSpeed, realRSpeed);

	// turn on motor A
	if (realLSpeed >= 0)
	{
		digitalWrite(input1, HIGH);
		digitalWrite(input2, LOW);
		analogWrite(enableA, realLSpeed);
	}
	else
	{
		digitalWrite(input1, LOW);
		digitalWrite(input2, HIGH);
		analogWrite(enableA, -realLSpeed);
	}


	// turn on motor B
	if (realRSpeed >= 0)
	{
		digitalWrite(input3, HIGH);
		digitalWrite(input4, LOW);
		analogWrite(enableB, realRSpeed);
	}
	else
	{
		digitalWrite(input3, LOW);
		digitalWrite(input4, HIGH);
		analogWrite(enableB, -realRSpeed);
	}
}

