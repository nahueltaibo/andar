#ifndef AndarMotors_h
#define AndarMotors_h

#include "Backbone.h"
#include "L298NMotors.h"
#include "MotorsMessage.h"

class AndarMotors : public L298NMotors
{
public:
	AndarMotors();
	~AndarMotors();
	void processMotors(MotorsMessage motorsMessage);
};

#endif