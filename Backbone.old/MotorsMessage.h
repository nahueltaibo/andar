#ifndef MotorsMessage_h
#define MotorsMessage_h

#include "Message.h"
#include "Backbone.h"

class MotorsMessage :
	public Message
{
public:
	MotorsMessage(Message base);
	~MotorsMessage();

	AndarMessages getType();
	byte getAcceleration();
	int getLeftSpeed();
	int getRightSpeed();

private:
	AndarMessages type;
	byte acceleration = 0;
	int leftSpeed = 0;
	int rightSpeed = 0;
};

#endif