package com.nt.andar.infrastructure.messaging;

/**
 * Created by Nahuel on 2015-11-22.
 */
public enum MessageType {
    //Message types
    NoMessage(0),
    Ping(1),
    Pong(2),
    Publish(3),
    Disconnect(4),
    HealthSequence(5);

    private int numVal;

    MessageType(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }

    public static MessageType fromShort(short x) {
        switch (x) {
            case 0:
                return NoMessage;
            case 1:
                return Ping;
            case 2:
                return Pong;
            case 3:
                return Publish;
            case 4:
                return Disconnect;
            case 5:
                return HealthSequence;
        }
        return null;
    }
}
