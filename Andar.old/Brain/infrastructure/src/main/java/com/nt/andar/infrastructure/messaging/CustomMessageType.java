package com.nt.andar.infrastructure.messaging;

/**
 * Created by Nahuel on 2015-11-28.
 */
public enum CustomMessageType {
    //Message types
    Motors(0);

    private int numVal;

    CustomMessageType(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }

    public static CustomMessageType fromShort(short x) {
        switch (x) {
            case 1:
                return Motors;
        }
        return null;
    }
}