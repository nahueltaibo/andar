package com.nt.andar.infrastructure.accessories;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.ParcelFileDescriptor;
import android.util.Log;

import com.nt.andar.infrastructure.messaging.Message;
import com.nt.andar.infrastructure.messaging.MessageImpl;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Nahuel on 2015-11-28.
 */
public class Accessory {
    private static final String ACTION_USB_PERMISSION = "com.google.android.DemoKit.action.USB_PERMISSION";
    private static final String TAG = "Accessory";
    Context context;
    private UsbManager usbManager;
    private UsbAccessory mAccessory;
    private ParcelFileDescriptor fileDescriptor;
    private FileInputStream in;
    private FileOutputStream out;
    private boolean connected;
    private PendingIntent permissionIntent;
    private boolean mPermissionRequestPending;
    private OnConnectionEstablished connectionEstablishedEvent;
    private OnConnectionClosed connectionClosedEvent;
    private final OnMessageReceived messageReceivedEvent;
    private MonitoringThread monitorThread;

    public Accessory(
            Context context,
            OnConnectionEstablished connectionEstablishedEvent,
            OnConnectionClosed connectionClosedEvent,
            OnMessageReceived messageReceivedEvent) {

        this.context = context;
        this.connectionEstablishedEvent = connectionEstablishedEvent;
        this.connectionClosedEvent = connectionClosedEvent;
        this.messageReceivedEvent = messageReceivedEvent;


        //Let Android know we want to know about permissions changes on USB
        permissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);

        //Create an intent filter to receive intents when there is a change on USB permissions or
        //the Accessory has been detached
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);

        context.registerReceiver(mUsbReceiver, filter);

        //Get the USB Manager
        usbManager = (UsbManager) context.getSystemService(context.USB_SERVICE);

        UsbAccessory[] accessories = usbManager.getAccessoryList();
        UsbAccessory accessory = (accessories == null ? null : accessories[0]);
        if (accessory != null) {
            if (usbManager.hasPermission(accessory)) {
                openAccessory(accessory);
            } else {
                synchronized (mUsbReceiver) {
                    if (!mPermissionRequestPending) {
                        Log.d(TAG, "Requesting permission to accessory...");
                        usbManager.requestPermission(accessory, permissionIntent);
                        mPermissionRequestPending = true;
                    }
                }
            }
        } else {
            Log.d(TAG, "There is no Accessory connected.");
        }
    }

    private boolean openAccessory(UsbAccessory accessory) {
        fileDescriptor = usbManager.openAccessory(accessory);

        if (fileDescriptor != null) {
            mAccessory = accessory;
            FileDescriptor mFileDescriptor = fileDescriptor.getFileDescriptor();
            in = new FileInputStream(mFileDescriptor);
            out = new FileOutputStream(mFileDescriptor);
            Log.i(TAG, "Connected to Accessory.");

            monitorThread = new MonitoringThread();
            Thread thread = new Thread(null, monitorThread, TAG);
            thread.start();

            connected = true;
            connectionEstablishedEvent.connectionEstablished();
        } else {
            Log.e(TAG, "Failed to open accessory.");
            return true;
        }
        return false;
    }

    public void closeAccessory() {
        try {
            if (fileDescriptor != null) {
                fileDescriptor.close();
            }
        } catch (Exception e) {
            Log.d(TAG, "Unable to close fileDescriptor");
        }
        try {
            if (mUsbReceiver != null) {
                context.unregisterReceiver(mUsbReceiver);
            }
        } catch (Exception e) {
            Log.d(TAG, "Unable to unregister UsbReceiver");
        }

        fileDescriptor = null;
        mUsbReceiver = null;
        mAccessory = null;
        in = null;
        out = null;
        connected = false;

        //Fire the event informing that the connection was closed
        connectionClosedEvent.connectionClosed();
    }

    public InputStream GetInputStream() {
        return in;
    }

    public OutputStream GetOutputStream() {
        return out;
    }

    /**
     * Sends a Message object to Backbone
     * Returns true if the message was sent, false otherwise
     */
    public boolean sendMessage(Message message) {
        if (GetOutputStream() != null) {
            try {
                Log.d(TAG, String.format("Brain -> Backbone %s", message.toString()));
                message.send(GetOutputStream());
                return true;
            } catch (IOException e) {
                Log.e(TAG, "Connection lost with Accessory", e);
                connectionClosedEvent.connectionClosed();
                return connected = false;
            }
        }
        return false;
    }

    public Message receiveMessage() throws IOException {
        try {
            return new MessageImpl(GetInputStream());
        } catch (IOException e) {
            Log.e(TAG, "Connection with Accessory lost.", e);
            connected = false;
            throw e;
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public interface OnConnectionEstablished {
        void connectionEstablished();
    }

    public interface OnConnectionClosed {
        void connectionClosed();
    }

    public interface OnMessageReceived {
        void messageReceived(Message msg);
    }

    private BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                Log.v(TAG, "ACTION_USB_PERMISSION received");
                synchronized (this) {
                    UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        openAccessory(accessory);
                    } else {
                        Log.d(TAG, "permission denied for accessory " + accessory);
                    }
                    mPermissionRequestPending = false;
                }
            } else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
                Log.v(TAG, "ACTION_USB_ACCESSORY_DETACHED received");

                UsbAccessory accessory = intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                if (accessory != null && accessory.equals(mAccessory)) {
                    closeAccessory();
                }
            }
        }
    };

    private class MonitoringThread implements Runnable {
        public void run() {
            try {
                while (!Thread.interrupted()) {
                    Message msg = receiveMessage();
                    Log.v(TAG, "Message received " + msg);
                    messageReceivedEvent.messageReceived(msg);
                }
            } catch (IOException e) {
                Log.e(TAG, "Connection lost with Bakbone", e);
                closeAccessory();
            } catch (Exception e) {
                Log.e(TAG, "Error receiving from Backbone", e);
                closeAccessory();
            }
        }
    }
}

