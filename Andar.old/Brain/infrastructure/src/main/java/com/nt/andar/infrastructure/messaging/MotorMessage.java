package com.nt.andar.infrastructure.messaging;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Nahuel on 2015-11-28.
 */
public class MotorMessage extends MessageImpl {
    private int acceleration = 0;
    private int leftSpeed = 0;
    private int rightSpeed = 0;


    public MotorMessage() {
        super(MessageType.Publish);
    }

    public MotorMessage(int acceleration, int leftSpeed, int rightSpeed) {
        super(MessageType.Publish);
        this.acceleration = acceleration;
        this.leftSpeed = leftSpeed;
        this.rightSpeed = rightSpeed;
    }

    public MotorMessage(InputStream readFrom) throws IOException {
        super(readFrom);
    }

    public MotorMessage(byte[] buffer) {
        super(buffer);
    }

    public void setAcceleration(int acceleration) {
        if (acceleration < 0 || acceleration > 100) {
            throw new IllegalArgumentException("acceleration should be between 0 and 100");
        }
        this.acceleration = acceleration;
    }

    public void setLeftSpeed(int leftSpeed) {
        if (leftSpeed < -100 || leftSpeed > 100) {
            throw new IllegalArgumentException("leftSpeed should be between -100 and 100");
        }
        this.leftSpeed = leftSpeed;
    }

    public void setRightSpeed(int rightSpeed) {
        if (leftSpeed < -100 || leftSpeed > 100) {
            throw new IllegalArgumentException("rightSpeed should be between -100 and 100");
        }
        this.rightSpeed = rightSpeed;
    }

    @Override
    public byte[] getBytes() {
        //Reset the payload, just in case getBytes is called more than once
        this.resetPayload();

        //Add the parameters in the correct order to the payload
        //add Andar's message Type
        pushByte(CustomMessageType.Motors.getNumVal());

        //add the Acceleration (0 to 100)
        pushByte((byte)this.acceleration);

        //add the left motor speed (-100 to 100)
        pushInt((short) this.leftSpeed);

        //add the right motor speed (-100 to 100)
        pushInt((short) this.rightSpeed);

        return super.getBytes();
    }
}

