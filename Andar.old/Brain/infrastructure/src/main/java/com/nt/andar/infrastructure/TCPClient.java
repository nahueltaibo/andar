package com.nt.andar.infrastructure;

import android.util.Log;

import com.nt.andar.infrastructure.messaging.Message;
import com.nt.andar.infrastructure.messaging.MessageImpl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by Nahuel on 2015-11-28.
 */
public class TCPClient implements Runnable {

    private static final String TAG = "TCPClient";
    public static final int SERVERPORT = 5657;
    private String host;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;

    OutputStream out;
    InputStream in;

    /**
     * Constructor of the class. OnMessagedReceived listens for the messages received from server
     */
    public TCPClient(String host, OnMessageReceived listener) {
        this.host = host;
        mMessageListener = listener;
    }

    /**
     * Sends a message to the server
     */
    public boolean sendMessage(Message message) {
        if (out != null) {
            try {
                Log.d(TAG, String.format("Remote -> %s", message.toString()));
                out.write(message.getBytes());
                out.flush();
                return true;
            } catch (IOException e) {
                Log.e(TAG, "write failed", e);
                return false;
            }
        }
        return false;
    }

    public void stopClient() {
        mRun = false;
    }

    public void run() {

        mRun = true;

        try {
            InetAddress serverAddr = InetAddress.getByName(host);

            Log.e("TCP Client", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVERPORT);

            try {
                //send the message to the server
                out = socket.getOutputStream();

                //receive the message which the server sends back
                in = socket.getInputStream();

                //in this where the client listens for the messages sent by the server
                while (mRun && socket.isConnected()) {
                    try {
                        //Create a Message from what we receive from Brain
                        Message msg = new MessageImpl(in);

                        Log.d(TAG, String.format("Remote <- Brain %s", msg.toString()));

                        //Let Remote know about this message
                        mMessageListener.messageReceived(msg);
                    } catch (Exception ex) {
                        Log.e(TAG, "Error receiving from Brain", ex);
                    }
                }

            } catch (Exception e) {
                Log.e("TCP", "Error sending", e);
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {

            Log.e("TCP", "Error connecting", e);
        }
    }

    //Declare the interface. The method messageReceived(String message) will must be implemented in the MyActivity
    //class at on asynckTask doInBackground
    public interface OnMessageReceived {
        void messageReceived(Message message);
    }
}

