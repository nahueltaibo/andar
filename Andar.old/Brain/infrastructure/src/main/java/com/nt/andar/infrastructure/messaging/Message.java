package com.nt.andar.infrastructure.messaging;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Nahuel on 2015-11-22.
 */
public interface Message {

    //For some reason, sending the message fails if it is more than 64 bytes in total (so 62 of payload)
    short MAX_MESSAGE_LENGTH = 64;
    short HEADER_LENGTH = 2;
    short MAX_PAYLOAD_LENGTH = MAX_MESSAGE_LENGTH - HEADER_LENGTH;

    //Arduino Data type sizes
    short ARDUINO_BYTE_SIZE = 1;
    short ARDUINO_INT_SIZE = 2;
    short ARDUINO_CHAR_SIZE = 1;
    short ARDUINO_INT_MIN = -32768;
    short ARDUINO_INT_MAX = 32767;

    MessageType getType();

    short getPayloadLength();

    short getTotalLength();

    boolean pushByte(int arduinoByte);

    boolean pushInt(short arduinoInt);

    boolean pushChar(char arduinoChar);

    short popByte();

    short popInt();

    char popChar();

    /**
     * Returns the byte[] array representing this message
     **/
    byte[] getBytes();

    /**
     * Resets the message Payload , so Payload is empty and Pos is 0
     **/
    void resetPayload();

    /**
     * Rewinds the message Payload , so Payload is the same than it was before, and Pos is 0
     **/
    void rewindPayload();

    void send(OutputStream out) throws IOException;
}
