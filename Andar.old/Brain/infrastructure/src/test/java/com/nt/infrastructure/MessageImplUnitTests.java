package com.nt.infrastructure;

import com.nt.andar.infrastructure.messaging.Message;
import com.nt.infrastructure.messaging.MessageImpl;
import com.nt.andar.infrastructure.messaging.MessageType;

import junit.framework.Assert;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class MessageImplUnitTests {
    @Test
    public void createWithMessageTypeTest() throws Exception {
        Message message = new MessageImpl((short) MessageType.Ping.getNumVal());

        assertEquals((short) MessageType.Ping.getNumVal(), message.getBytes()[0]);
    }

    @Test
    public void createWithBytearrayTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 0});

        assertEquals((short) MessageType.Ping.getNumVal(), message.getBytes()[0]);
    }

    @Test
    public void createWithInputStreamTest() throws Exception {
        ByteArrayInputStream stream = new ByteArrayInputStream(new byte[]{(byte) MessageType.Ping.getNumVal(), 0});

        Message message = new MessageImpl((InputStream) stream);

        assertEquals((short) MessageType.Ping.getNumVal(), message.getBytes()[0]);
    }

    @Test(expected = IOException.class)
    public void createWithInputStreamBrokenTest() throws Exception {
        ByteArrayInputStream stream = new ByteArrayInputStream(new byte[]{(byte) MessageType.Ping.getNumVal(), 2, 1});

        Message message = new MessageImpl((InputStream) stream);
    }

    @Test
    public void getByteTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 1, 1});

        assertEquals(MessageType.Ping, message.getType());
    }

    @Test
    public void getPayloadLengthTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 2, 1, 2});

        assertEquals(2, message.getPayloadLength());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void pushByteTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 0});

        assertEquals(0, message.getPayloadLength());

        //min value
        message.pushByte(0);
        assertEquals(1, message.getPayloadLength());
        assertEquals(0, message.getBytes()[2]);

        //max value
        message.pushByte(255);
        assertEquals(2, message.getPayloadLength());
        assertEquals(255, message.getBytes()[3] & 0xFF);

        //invalid value
        message.pushByte(256);
    }

    @Test
    public void pushIntTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 0});

        assertEquals(0, message.getPayloadLength());

        short value = -32768;
        byte data1 = (byte) (value & 0xFF);
        byte data2 = (byte) ((value >> 8) & 0xFF);

        //min value
        message.pushInt(value);
        assertEquals(2, message.getPayloadLength());
        assertEquals(data1, message.getBytes()[2]);
        assertEquals(data2, message.getBytes()[3]);


        value = 32767;
        data1 = (byte) (value & 0xFF);
        data2 = (byte) ((value >> 8) & 0xFF);

        //max value
        message.pushInt(value);
        assertEquals(4, message.getPayloadLength());
        assertEquals(data1, message.getBytes()[4]);
        assertEquals(data2, message.getBytes()[5]);
    }

    @Test
    public void pushCharTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 0});

        assertEquals(0, message.getPayloadLength());

        //min value
        message.pushChar('A');
        assertEquals(1, message.getPayloadLength());
        assertEquals('A', message.getBytes()[2]);

        //max value
        message.pushByte('0');
        assertEquals(2, message.getPayloadLength());
        assertEquals('0', message.getBytes()[3]);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void popByteTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 2, 0, (byte) 255});

        //min value
        assertEquals(0, message.popByte());

        //max value
        assertEquals(255, message.popByte() & 0xFF);

        //should be no more values
        message.popByte();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void popIntTest() throws Exception {

        ByteBuffer first = ByteBuffer.allocate(2);
        first.order(ByteOrder.LITTLE_ENDIAN);
        first.putShort((short) -32768);
        first.flip();

        ByteBuffer second = ByteBuffer.allocate(2);
        second.order(ByteOrder.LITTLE_ENDIAN);
        second.putShort((short) 32767);
        second.flip();

        Message message = new MessageImpl(new byte[]{
                (byte) MessageType.Ping.getNumVal(),
                4,
                first.get(0),
                first.get(1),
                second.get(0),
                second.get(1)});

        //min value
        assertEquals(-32768, message.popInt());

        //max value
        assertEquals(32767, message.popInt());

        //should be no more values
        message.popInt();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void popCharTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 2, (byte) 'A', (byte) '0'});

        //min value
        assertEquals('A', message.popChar());

        //max value
        assertEquals('0', message.popChar());

        //should be no more values
        message.popChar();
    }

    @Test
    public void getTotalLengthTest() throws Exception {
        Message message = new MessageImpl(new byte[]{(byte) MessageType.Ping.getNumVal(), 4, 1, 2, 3, 4});

        assertEquals(2 + 4, message.getTotalLength());
    }

    @Test
    public void getBytesTest() throws Exception {
        Message message = new MessageImpl((short)MessageType.Ping.getNumVal());

        byte[] bytes = message.getBytes();
        assertEquals((short)MessageType.Ping.getNumVal(), bytes[0]);
        assertEquals(0, bytes[1]);

        message.pushByte(123);
        bytes = message.getBytes();
        assertEquals((short)MessageType.Ping.getNumVal(), bytes[0]);
        assertEquals(1, bytes[1]);
        assertEquals(123, bytes[2]);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void resetPayloadTest() throws Exception {
        Message message = new MessageImpl((short)MessageType.Ping.getNumVal());

        message.pushByte(123);

        message.resetPayload();

        byte[] bytes = message.getBytes();
        assertEquals((short) MessageType.Ping.getNumVal(), bytes[0]);
        assertEquals(0, bytes[1]);

        message.popByte();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void rewindPayloadTest() throws Exception {
        Message message = new MessageImpl((short)MessageType.Ping.getNumVal());

        message.pushByte(123);
        message.pushInt((short) 1223);
        message.pushChar('R');

        assertEquals(123, message.popByte());
        assertEquals((short) 1223, message.popInt());

        message.rewindPayload();

        assertEquals(123, message.popByte());
        assertEquals((short) 1223, message.popInt());
        assertEquals('R', message.popChar());

        message.popByte();
    }

    @Test
    public void sendTest() throws Exception {
        Message message = new MessageImpl((short)MessageType.Ping.getNumVal());
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        message.pushByte(123);
        message.pushChar('R');

        message.send(output);

        byte[] byteOutput = output.toByteArray();

        Assert.assertEquals((short) MessageType.Ping.getNumVal(), byteOutput[0]);
        Assert.assertEquals(2, byteOutput[1]);
        Assert.assertEquals(123, byteOutput[2]);
        Assert.assertEquals((byte) 'R', byteOutput[3]);
    }
}