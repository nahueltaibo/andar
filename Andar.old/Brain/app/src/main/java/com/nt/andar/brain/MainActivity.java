package com.nt.andar.brain;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.nt.andar.infrastructure.accessories.Accessory;
import com.nt.andar.infrastructure.messaging.Message;
import com.nt.andar.infrastructure.messaging.MotorMessage;

import net.majorkernelpanic.streaming.gl.SurfaceView;

public class MainActivity extends AppCompatActivity {
    private Accessory backbone;
    private RemoteBridge remote;
    private StreamingServer streamingServer;

    private static final String TAG = "MainActivity";
    private ImageView backboneStatus;
    private ImageView remoteStatus;
    private ImageView videoStreamStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Keep the screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);
        backboneStatus = (ImageView) findViewById(R.id.backboneStatus);
        remoteStatus = (ImageView) findViewById(R.id.remoteStatus);
        videoStreamStatus = (ImageView) findViewById(R.id.videoStreamingStatus);

        Log.d(TAG, "Starting Accessory...");

        backbone = new Accessory(this,
                new Accessory.OnConnectionEstablished() {
                    @Override
                    public void connectionEstablished() {
                        connectionWithBackBoneEstablished();
                    }
                },
                new Accessory.OnConnectionClosed() {
                    @Override
                    public void connectionClosed() {
                        connectionWithBackboneClosed();
                    }
                },
                new Accessory.OnMessageReceived() {
                    @Override
                    public void messageReceived(Message msg) {
                        processMessageFromBackbone(msg);
                    }
                });

        // Start the thread that listens to Backbone messages
        //StartMonitorThread();

        // Start the Server that listens for a connection from the Remote
        StartRemoteBridge();

        // Start the video and audio streaming server
        StartStreamingServer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        backbone.closeAccessory();
    }

    private void StartStreamingServer() {
        Log.d(TAG, "Starting Streaming server...");
        streamingServer = new StreamingServer(this, (SurfaceView) findViewById(R.id.surface), new StreamingServer.OnVideoStreamingStarted() {
            @Override
            public void videoStreamingStarted() {
                videoStreamStatus.setImageResource(R.drawable.green);
            }
        }, new StreamingServer.OnVideoStreamingStopped() {
            @Override
            public void videoStreamingStopped() {
                videoStreamStatus.setImageResource(R.drawable.red);
            }
        });
    }

    private void StartRemoteBridge() {

        Log.d(TAG, "Starting Remote bridge");

        remote = new RemoteBridge(new RemoteBridge.OnConnectionAccepted() {
            @Override
            public void connectionAccepted() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        remoteStatus.setImageResource(R.drawable.green);
                    }
                });
            }
        }, new RemoteBridge.OnConnectionClosed() {
            @Override
            public void connectionClosed() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        remoteStatus.setImageResource(R.drawable.red);
                    }
                });
            }
        }, new RemoteBridge.OnMessageReceived() {
            @Override
            //here the messageReceived method is implemented
            public void messageReceived(Message message) {
                //this method calls the onProgressUpdate
                processMessageFromRemote(message);
            }
        });
    }

    public void forward(View view) {
        MotorMessage motors = new MotorMessage(100, 100, 100);
        backbone.sendMessage(motors);
    }

    public void left(View view) {
        MotorMessage motors = new MotorMessage(100, -100, 100);
        backbone.sendMessage(motors);
    }

    public void stop(View view) {
        MotorMessage motors = new MotorMessage(100, 0, 0);
        backbone.sendMessage(motors);
    }

    public void right(View view) {
        MotorMessage motors = new MotorMessage(100, 100, -100);
        backbone.sendMessage(motors);
    }

    public void backward(View view) {
        MotorMessage motors = new MotorMessage(100, -100, -100);
        backbone.sendMessage(motors);
    }

    private void connectionWithBackboneClosed() {
        Log.d(TAG, "OnConnectionClosed");
        backboneStatus.setImageResource(R.drawable.red);
    }

    private void connectionWithBackBoneEstablished() {
        Log.d(TAG, "OnConnectionEstablished");
        backboneStatus.setImageResource(R.drawable.green);
    }

    private void processMessageFromBackbone(Message message) {
        Log.d(TAG, "Brain <- Backbone: " + message.toString());
        remote.sendMessage(message);
    }

    private void processMessageFromRemote(Message message) {
        Log.d(TAG, "Brain <- Remote: " + message.toString());
        backbone.sendMessage(message);
    }

}
