package com.nt.andar.brain;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.preference.PreferenceManager;

import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.rtsp.RtspServer;
import net.majorkernelpanic.streaming.video.VideoQuality;

/**
 * Created by Nahuel on 2015-11-29.
 */
public class StreamingServer {
    private final SurfaceView surfaceView;
    private final OnVideoStreamingStarted videoStreamingStartedEvent;
    private final OnVideoStreamingStopped videoStreamingFinishedEvent;

    public StreamingServer(Context context, SurfaceView view, final OnVideoStreamingStarted videoStreamingStarted, final OnVideoStreamingStopped videoStreamingStopped) {

        surfaceView = view;
        videoStreamingStartedEvent = videoStreamingStarted;
        videoStreamingFinishedEvent = videoStreamingStopped;

        // Sets the port of the RTSP server to 5658
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(RtspServer.KEY_PORT, String.valueOf(5658));
        editor.commit();

        // Configures the SessionBuilder
        SessionBuilder.getInstance()
                .setSurfaceView(surfaceView)
                .setContext(context.getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(16000, 32000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
                .setVideoQuality(new VideoQuality(320, 240, 20, 500000))
                .setCamera(Camera.CameraInfo.CAMERA_FACING_FRONT)
                .setCallback(new Session.Callback() {
                    @Override
                    public void onBitrateUpdate(long bitrate) {

                    }

                    @Override
                    public void onSessionError(int reason, int streamType, Exception e) {
                        videoStreamingStopped.videoStreamingStopped();
                    }

                    @Override
                    public void onPreviewStarted() {

                    }

                    @Override
                    public void onSessionConfigured() {

                    }

                    @Override
                    public void onSessionStarted() {
                        videoStreamingStarted.videoStreamingStarted();
                    }

                    @Override
                    public void onSessionStopped() {
                        videoStreamingStopped.videoStreamingStopped();
                    }
                });

        // Starts the RTSP server
        context.startService(new Intent(context, RtspServer.class));
    }

    public interface OnVideoStreamingStarted {
        void videoStreamingStarted();
    }

    public interface OnVideoStreamingStopped {
        void videoStreamingStopped();
    }

}
