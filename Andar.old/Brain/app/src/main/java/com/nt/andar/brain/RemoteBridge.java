package com.nt.andar.brain;

import android.util.Log;

import com.nt.andar.infrastructure.messaging.Message;
import com.nt.andar.infrastructure.messaging.MessageImpl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Nahuel on 2015-11-28.
 */
public class RemoteBridge implements Runnable {
    private static final String TAG = "RemoteBridge";
    private final OnConnectionAccepted connectionAcceptedEvent;
    private final OnConnectionClosed connectionClosedEvent;
    private OnMessageReceived onMessageReceivedEvent;

    ServerSocket serverSocket;
    int port = 5657;
    private InputStream in;
    private OutputStream out;
    private boolean shouldClose = false;

    public RemoteBridge(OnConnectionAccepted connectionAccepted, OnConnectionClosed connectionClosed, OnMessageReceived onReceivedFromRemote) {
        this.onMessageReceivedEvent = onReceivedFromRemote;
        this.connectionAcceptedEvent = connectionAccepted;
        this.connectionClosedEvent = connectionClosed;

        Thread thread = new Thread(null, this, "RemoteBridgeThread");
        thread.start();
    }

    @Override
    public void run() {
        try {
            Log.d(TAG, "Remote bridge started.");

            //Just to give time to old RemoteBridge instances to close properly
            Thread.sleep(2000);

            Log.v(TAG, "Openning Server for Remote connection...");
            serverSocket = new ServerSocket(port);

            while (!Thread.interrupted() && !shouldClose) {
                //wait for a connection from the Remote and accept it
                Log.d(TAG, "Waiting for a connection from Remote.");
                Socket s = serverSocket.accept();

                Log.i(TAG, String.format("Remote connected from %s", s.getInetAddress().getHostAddress()));

                in = s.getInputStream();
                out = s.getOutputStream();

                //Fire the connection accepted event
                connectionAcceptedEvent.connectionAccepted();

                try {
                    while (!Thread.interrupted() && s.isConnected() && !shouldClose) {

                        //Create a Message object from what we receive from Remote.
                        Message msg = new MessageImpl(in);

                        //Let Brain know about the received message
                        onMessageReceivedEvent.messageReceived(msg);
                    }
                } catch (Exception ex) {
                    Log.i(TAG, "Connection lost with Remote.");
                } finally {

                    //Fire the connection closed event
                    connectionClosedEvent.connectionClosed();

                    if (s != null) {
                        try {
                            s.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.i(TAG, "Server Socket error.");
            e.printStackTrace();
        } finally {
            Log.i(TAG, "Closing RemoteBridge...");
            if (serverSocket != null) {
                try {
                    Log.i(TAG, "Closing Server Socket...");
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Sends a Message object to Remote
    //Returns true if the message was sent, false otherwise
    public boolean sendMessage(Message message) {
        if (out != null) {
            try {
                Log.d(TAG, String.format("Brain -> Remote : %s", message.toString()));
                out.write(message.getBytes());
                return true;
            } catch (IOException e) {
                Log.e(TAG, "write to Remote failed", e);
                return false;
            }
        }
        return false;
    }

    public void close() {
        shouldClose = true;
    }

    public interface OnConnectionAccepted {
        void connectionAccepted();
    }

    public interface OnConnectionClosed {
        void connectionClosed();
    }

    public interface OnMessageReceived {
        void messageReceived(Message message);
    }
}

