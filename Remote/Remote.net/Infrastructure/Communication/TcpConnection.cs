﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Infrastructure.Communication.Messaging;
using Infrastructure.Logging;

namespace Infrastructure.Communication
{
    public class TcpConnection : IDisposable
    {
        private TcpClient tcpClient;
        private readonly ILog log;

        public TcpConnection(string host, int port)
        {
            Host = host;
            Port = port;
            log = Log.Instance;
        }

        public Task Receiver { get; private set; }
        public int Port { get; set; }
        public string Host { get; set; }
        public CancellationTokenSource ReceiverToken { get; set; }
        public NetworkStream Stream { get; set; }

        public void Dispose()
        {
            if (tcpClient != null)
            {
                log.Verbose("Closing connection with Brain...");
                tcpClient.Close();
            }
            if (Receiver != null)
            {
                log.Verbose("Closing receiver task...");
                ReceiverToken.Cancel();
            }
        }

        public void Connect()
        {
            try
            {
                tcpClient = new TcpClient();

                log.Verbose("Connecting...");
                tcpClient.Connect(Host, Port);

                log.Debug("Connected to Brain");

                Stream = tcpClient.GetStream();

                ReceiverToken = new CancellationTokenSource();
                var token = ReceiverToken.Token;
                Receiver = Task.Run(() => Receive(token), token);
            }

            catch (Exception e)
            {
                log.Error("Error..... " + e.StackTrace);
            }
        }

        private void Receive(CancellationToken ct)
        {
            try
            {
                while (!ct.IsCancellationRequested)
                {
                    var bb = new byte[100];

                    var k = Stream.Read(bb, 0, 100);

                    for (var i = 0; i < k; i++)
                    {
                        log.Verbose(Convert.ToChar(bb[i]).ToString());
                    }
                }
            }
            catch (IOException ex)
            {
                if (!ct.IsCancellationRequested)
                {
                    log.Error("Error receiving from Brain!");
                }
                else
                {
                    log.Debug("Receive Task Ended.");
                }
            }
        }
    }
}