﻿using System;
using System.IO;

namespace Infrastructure.Communication.Messaging
{
    public interface IMessage
    {
        MessageType Type { get; }
        byte PayloadLength { get; }
        byte TotalLength { get; }
        Byte[] Bytes { get; }
        bool PushByte(byte theByte);
        bool PushInt(short arduinoInt);
        bool PushChar(char arduinoChar);
        short PopByte();
        short PopInt();
        char PopChar();
        void ResetPayload();
        void RewindPayload();
        void Send(Stream output);
    }
}