﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Infrastructure.Communication.Messaging
{
    public class Message : IMessage
    {
        public const int HeaderLength = 2;
        public const int MaxPayloadLength = MaxMessageLength - HeaderLength;
        public const int MaxMessageLength = 64;
        //Keeps track of the current reading possition in the payload
        protected int pos;
        protected readonly IList<byte> Payload = new List<byte>();

        public Message(MessageType type)
        {
            Type = type;
        }

        public Message(byte[] bytes)
        {
            Type = (MessageType) bytes[0];
            for (var i = 0; i < bytes[1]; i++)
            {
                Payload.Add(bytes[HeaderLength + i]);
            }
        }

        public Message(Stream input)
        {
            Type = (MessageType) input.ReadByte();
            var payloadLength = (byte) input.ReadByte();

            for (var i = 0; i < payloadLength; i++)
            {
                Payload.Add((byte) input.ReadByte());
            }
        }

        public MessageType Type { get; set; }

        public virtual byte PayloadLength
        {
            get { return (byte) Payload.Count; }
        }

        public virtual byte TotalLength
        {
            get { return (byte) (HeaderLength + Payload.Count); }
        }

        public virtual byte[] Bytes
        {
            get
            {
                var bytes = new byte[HeaderLength + Payload.Count];
                bytes[0] = (byte) Type;
                bytes[1] = (byte) Payload.Count;
                for (var i = 0; i < Payload.Count; i++)
                {
                    bytes[i + HeaderLength] = Payload[i];
                }

                return bytes;
            }
        }

        public virtual bool PushByte(byte theByte)
        {
            if (EnoughSpace(sizeof (byte)))
            {
                Payload.Add(theByte);
                return true;
            }
            return false;
        }

        public virtual bool PushInt(short arduinoInt)
        {
            if (EnoughSpace(sizeof (short)))
            {
                var bytes = BitConverter.GetBytes(arduinoInt);
                Payload.Add(bytes[0]);
                Payload.Add(bytes[1]);
            }
            return false;
        }

        public virtual bool PushChar(char theChar)
        {
            if (EnoughSpace(sizeof (byte)))
            {
                Payload.Add((byte) theChar);
                return true;
            }
            return false;
        }

        public virtual short PopByte()
        {
            return Payload[pos++];
        }

        public virtual short PopInt()
        {
            var result = BitConverter.ToInt16(Payload.ToArray(), pos);
            pos += 2;
            return result;
        }

        public virtual char PopChar()
        {
            return (char) Payload[pos++];
        }

        public virtual void ResetPayload()
        {
            Payload.Clear();
            pos = 0;
        }

        public virtual void RewindPayload()
        {
            pos = 0;
        }

        public virtual void Send(Stream output)
        {
            foreach (var b in Bytes)
            {
                output.WriteByte(b);
            }
        }

        protected virtual bool EnoughSpace(byte sizeRequired)
        {
            return (MaxPayloadLength - PayloadLength) >= sizeRequired;
        }
    }
}