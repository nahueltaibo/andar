﻿namespace Infrastructure.Communication.Messaging
{
    public enum MessageType
    {
        NoMessage = 0,
        Ping = 1,
        Pong = 2,
        Publish = 3,
        Disconnect = 4
    }
}