﻿namespace Infrastructure.Logging
{
    public interface ILog
    {
        ITextConsoleLog TextConsole { set; }
        void Verbose(string text);
        void Debug(string text);
        void Info(string text);
        void Warning(string text);
        void Error(string text);
    }
}