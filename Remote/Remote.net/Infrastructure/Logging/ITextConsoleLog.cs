﻿namespace Infrastructure.Logging
{
    public interface ITextConsoleLog
    {
        void WriteLine(string text);
    }
}