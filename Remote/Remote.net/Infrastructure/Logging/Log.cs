﻿using System;

namespace Infrastructure.Logging
{
    public class Log : ILog
    {
        private static ILog _instance;
        private ITextConsoleLog textConsole;

        private Log()
        {
        }

        public static ILog Instance
        {
            get { return _instance ?? (_instance = new Log()); }
        }

        public ITextConsoleLog TextConsole
        {
            set { textConsole = value; }
        }

        public void Verbose(string text)
        {
            if (textConsole != null)
            {
                textConsole.WriteLine(text + Environment.NewLine);
            }
            Console.WriteLine(text);
        }

        public void Debug(string text)
        {
            if (textConsole != null)
            {
                textConsole.WriteLine(text + Environment.NewLine);
            }
            Console.WriteLine(text);
        }

        public void Info(string text)
        {
            if (textConsole != null)
            {
                textConsole.WriteLine(text + Environment.NewLine);
            }
            Console.WriteLine(text);
        }

        public void Warning(string text)
        {
            if (textConsole != null)
            {
                textConsole.WriteLine(text + Environment.NewLine);
            }
            Console.WriteLine(text);
        }

        public void Error(string text)
        {
            if (textConsole != null)
            {
                textConsole.WriteLine(text + Environment.NewLine);
            }
            Console.WriteLine(text);
        }

        private void WriteTotextBox(string text)
        {
            textConsole.WriteLine(text);
        }
    }
}