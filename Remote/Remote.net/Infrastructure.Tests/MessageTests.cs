﻿using System;
using System.IO;
using Infrastructure.Communication.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Infrastructure.Tests
{
    [TestClass]
    public class MessageTests
    {
        [TestMethod]
        public void CreateMessageWithMessageTypeTest()
        {
            IMessage message = new Message(MessageType.Ping);
            var bytes = message.Bytes;

            Assert.AreEqual((int) MessageType.Ping, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
        }

        [TestMethod]
        public void CreateMessageWithByteArrayTest()
        {
            byte[] bytes = {(byte) MessageType.Publish, 1, 2};

            IMessage message = new Message(bytes);
            var resultBytes = message.Bytes;

            Assert.AreEqual((byte) MessageType.Publish, resultBytes[0]);
            Assert.AreEqual(1, resultBytes[1]);
            Assert.AreEqual(2, resultBytes[2]);
        }

        [TestMethod]
        public void CreateMessageWithStreamTest()
        {
            byte[] bytes = {(byte) MessageType.Publish, 1, 2};
            var stream = new MemoryStream(bytes);

            IMessage message = new Message(stream);
            var resultBytes = message.Bytes;

            Assert.AreEqual((byte) MessageType.Publish, resultBytes[0]);
            Assert.AreEqual(1, resultBytes[1]);
            Assert.AreEqual(2, resultBytes[2]);
        }

        [TestMethod]
        public void GetTypeTest()
        {
            byte[] bytes = {(byte) MessageType.Pong, 0};

            IMessage message = new Message(bytes);

            Assert.AreEqual(MessageType.Pong, message.Type);
        }

        [TestMethod]
        public void PayloadLengthTest()
        {
            byte[] bytes = {(byte) MessageType.Publish, 4, 1, 2, 3, 4};

            IMessage message = new Message(bytes);

            Assert.AreEqual(4, message.PayloadLength);
        }

        [TestMethod]
        public void TotalLengthTest()
        {
            byte[] bytes = {(byte) MessageType.Publish, 4, 1, 2, 3, 4};

            IMessage message = new Message(bytes);

            Assert.AreEqual(6, message.TotalLength);
        }

        [TestMethod]
        public void GetCorrectBytesTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 4, 1, 2, 3, 4});

            var bytes = message.Bytes;

            Assert.AreEqual((byte) MessageType.Publish, bytes[0]);
            Assert.AreEqual(4, bytes[1]);
            Assert.AreEqual(1, bytes[2]);
            Assert.AreEqual(2, bytes[3]);
            Assert.AreEqual(3, bytes[4]);
            Assert.AreEqual(4, bytes[5]);
        }

        [TestMethod]
        public void PushByteTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 0});

            Assert.AreEqual(0, message.PayloadLength);

            message.PushByte(0);
            var bytes = message.Bytes;
            Assert.AreEqual(0, bytes[2]);

            message.PushByte(255);
            bytes = message.Bytes;
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(255, bytes[3]);
        }

        [TestMethod]
        public void PushIntTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 0});

            message.PushInt(-32768);
            var firstIntBytes = BitConverter.GetBytes(-32768);

            var bytes = message.Bytes;
            Assert.AreEqual(2, bytes[1]);
            Assert.AreEqual(firstIntBytes[0], bytes[2]);
            Assert.AreEqual(firstIntBytes[1], bytes[3]);

            message.PushInt(32767);
            var secondIntBytes = BitConverter.GetBytes(32767);
            bytes = message.Bytes;
            Assert.AreEqual(4, bytes[1]);
            Assert.AreEqual(firstIntBytes[0], bytes[2]);
            Assert.AreEqual(firstIntBytes[1], bytes[3]);
            Assert.AreEqual(secondIntBytes[0], bytes[4]);
            Assert.AreEqual(secondIntBytes[1], bytes[5]);
        }

        [TestMethod]
        public void PushCharTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 0});

            Assert.AreEqual(0, message.PayloadLength);

            message.PushChar('a');
            var bytes = message.Bytes;
            Assert.AreEqual('a', (char) bytes[2]);

            message.PushChar('9');
            bytes = message.Bytes;
            Assert.AreEqual('a', (char) bytes[2]);
            Assert.AreEqual('9', (char) bytes[3]);
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void PopByteTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 2, 0, 255});

            //Test lower limit
            Assert.AreEqual(0, message.PopByte());

            //Test upper limit
            Assert.AreEqual(255, message.PopByte());

            //Should fire an exception..
            message.PopByte();
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void PopIntTest()
        {
            var firstIntBytes = BitConverter.GetBytes(-32768);
            var secondIntBytes = BitConverter.GetBytes(32767);

            IMessage message = new Message(new byte[]
            {
                (byte) MessageType.Publish,
                4,
                firstIntBytes[0],
                firstIntBytes[1],
                secondIntBytes[0],
                secondIntBytes[1]
            });

            Assert.AreEqual(4, message.PayloadLength);
            Assert.AreEqual(-32768, message.PopInt());
            Assert.AreEqual(32767, message.PopInt());

            //Thows an exception
            message.PopInt();
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void PopCharTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 2, (byte) 'A', (byte) '0'});

            //Test lower limit
            Assert.AreEqual('A', message.PopChar());

            //Test upper limit
            Assert.AreEqual('0', message.PopChar());

            //Should fire an exception..
            message.PopChar();
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentOutOfRangeException))]
        public void ResetPayloadTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 2, 0, 255});
            var bytes = message.Bytes;

            Assert.AreEqual((byte) MessageType.Publish, bytes[0]);
            Assert.AreEqual(2, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(255, bytes[3]);

            message.ResetPayload();
            bytes = message.Bytes;
            Assert.AreEqual((byte) MessageType.Publish, bytes[0]);
            Assert.AreEqual(0, bytes[1]);
            Assert.AreEqual(0, message.PayloadLength);
            message.PopByte();
        }

        [TestMethod]
        public void RewindPayloadTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 2, 0, 255});
            var bytes = message.Bytes;

            Assert.AreEqual((byte) MessageType.Publish, bytes[0]);
            Assert.AreEqual(2, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(255, bytes[3]);

            message.RewindPayload();
            bytes = message.Bytes;
            Assert.AreEqual((byte) MessageType.Publish, bytes[0]);
            Assert.AreEqual(2, bytes[1]);
            Assert.AreEqual(0, bytes[2]);
            Assert.AreEqual(0, message.PopByte());
            Assert.AreEqual(255, bytes[3]);
            Assert.AreEqual(255, message.PopByte());
        }

        [TestMethod]
        public void SendTest()
        {
            IMessage message = new Message(new byte[] {(byte) MessageType.Publish, 2, 0, 255});

            var stream = new MemoryStream();

            message.Send(stream);
            var sentBytes = stream.GetBuffer();

            Assert.AreEqual((byte) MessageType.Publish, sentBytes[0]);
            Assert.AreEqual(2, sentBytes[1]);
            Assert.AreEqual(0, sentBytes[2]);
            Assert.AreEqual(255, sentBytes[3]);
        }
    }
}