﻿namespace Remote.net
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabStatus = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pingBtn = new System.Windows.Forms.Button();
            this.ConsoleTxt = new System.Windows.Forms.TextBox();
            this.tabRemoteControl = new System.Windows.Forms.TabPage();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.video = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.audioEnabledChk = new System.Windows.Forms.CheckBox();
            this.StartVideoBtn = new System.Windows.Forms.Button();
            this.ConnectBtn = new System.Windows.Forms.Button();
            this.hostTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNetworkCaching = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabRemoteControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.ConnectBtn);
            this.splitContainer1.Panel2.Controls.Add(this.hostTxt);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.splitContainer1.Size = new System.Drawing.Size(1006, 721);
            this.splitContainer1.SplitterDistance = 734;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabStatus);
            this.tabControl1.Controls.Add(this.tabRemoteControl);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(734, 721);
            this.tabControl1.TabIndex = 0;
            // 
            // tabStatus
            // 
            this.tabStatus.Controls.Add(this.groupBox1);
            this.tabStatus.Controls.Add(this.ConsoleTxt);
            this.tabStatus.Location = new System.Drawing.Point(4, 25);
            this.tabStatus.Name = "tabStatus";
            this.tabStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabStatus.Size = new System.Drawing.Size(726, 692);
            this.tabStatus.TabIndex = 1;
            this.tabStatus.Text = "Status";
            this.tabStatus.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.pingBtn);
            this.groupBox1.Location = new System.Drawing.Point(520, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 46);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Messages";
            // 
            // pingBtn
            // 
            this.pingBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.pingBtn.Location = new System.Drawing.Point(3, 18);
            this.pingBtn.Name = "pingBtn";
            this.pingBtn.Size = new System.Drawing.Size(194, 23);
            this.pingBtn.TabIndex = 0;
            this.pingBtn.Text = "Send PING";
            this.pingBtn.UseVisualStyleBackColor = true;
            this.pingBtn.Click += new System.EventHandler(this.pingBtn_Click);
            // 
            // ConsoleTxt
            // 
            this.ConsoleTxt.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ConsoleTxt.Location = new System.Drawing.Point(3, 330);
            this.ConsoleTxt.Multiline = true;
            this.ConsoleTxt.Name = "ConsoleTxt";
            this.ConsoleTxt.Size = new System.Drawing.Size(720, 359);
            this.ConsoleTxt.TabIndex = 0;
            // 
            // tabRemoteControl
            // 
            this.tabRemoteControl.Controls.Add(this.splitContainer2);
            this.tabRemoteControl.Location = new System.Drawing.Point(4, 25);
            this.tabRemoteControl.Name = "tabRemoteControl";
            this.tabRemoteControl.Padding = new System.Windows.Forms.Padding(3);
            this.tabRemoteControl.Size = new System.Drawing.Size(726, 692);
            this.tabRemoteControl.TabIndex = 0;
            this.tabRemoteControl.Text = "RemoteControl";
            this.tabRemoteControl.UseVisualStyleBackColor = true;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(3, 3);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.video);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.txtNetworkCaching);
            this.splitContainer2.Panel2.Controls.Add(this.label3);
            this.splitContainer2.Panel2.Controls.Add(this.audioEnabledChk);
            this.splitContainer2.Panel2.Controls.Add(this.label2);
            this.splitContainer2.Panel2.Controls.Add(this.StartVideoBtn);
            this.splitContainer2.Size = new System.Drawing.Size(720, 686);
            this.splitContainer2.SplitterDistance = 631;
            this.splitContainer2.TabIndex = 0;
            // 
            // video
            // 
            this.video.Dock = System.Windows.Forms.DockStyle.Fill;
            this.video.Location = new System.Drawing.Point(0, 0);
            this.video.Name = "video";
            this.video.Size = new System.Drawing.Size(720, 631);
            this.video.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(555, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Use WASD keys to drive";
            // 
            // audioEnabledChk
            // 
            this.audioEnabledChk.AutoSize = true;
            this.audioEnabledChk.Location = new System.Drawing.Point(127, 2);
            this.audioEnabledChk.Name = "audioEnabledChk";
            this.audioEnabledChk.Size = new System.Drawing.Size(66, 21);
            this.audioEnabledChk.TabIndex = 1;
            this.audioEnabledChk.Text = "Audio";
            this.audioEnabledChk.UseVisualStyleBackColor = true;
            // 
            // StartVideoBtn
            // 
            this.StartVideoBtn.Dock = System.Windows.Forms.DockStyle.Left;
            this.StartVideoBtn.Location = new System.Drawing.Point(0, 0);
            this.StartVideoBtn.Name = "StartVideoBtn";
            this.StartVideoBtn.Size = new System.Drawing.Size(118, 51);
            this.StartVideoBtn.TabIndex = 0;
            this.StartVideoBtn.Text = "Start video";
            this.StartVideoBtn.UseVisualStyleBackColor = true;
            this.StartVideoBtn.Click += new System.EventHandler(this.ConnectStreamingBtn_Click);
            // 
            // ConnectBtn
            // 
            this.ConnectBtn.Dock = System.Windows.Forms.DockStyle.Top;
            this.ConnectBtn.Location = new System.Drawing.Point(0, 48);
            this.ConnectBtn.Name = "ConnectBtn";
            this.ConnectBtn.Size = new System.Drawing.Size(268, 30);
            this.ConnectBtn.TabIndex = 2;
            this.ConnectBtn.Text = "Connect";
            this.ConnectBtn.UseVisualStyleBackColor = true;
            this.ConnectBtn.Click += new System.EventHandler(this.ConnectBtn_Click);
            // 
            // hostTxt
            // 
            this.hostTxt.Dock = System.Windows.Forms.DockStyle.Top;
            this.hostTxt.Location = new System.Drawing.Point(0, 26);
            this.hostTxt.Name = "hostTxt";
            this.hostTxt.Size = new System.Drawing.Size(268, 22);
            this.hostTxt.TabIndex = 1;
            this.hostTxt.Text = "192.168.1.12";
            this.hostTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.hostTxt_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 5, 0, 4);
            this.label1.Size = new System.Drawing.Size(77, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Andar host";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(124, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 17);
            this.label3.TabIndex = 2;
            this.label3.Tag = "Miliseconds that the video will be delayed while caching";
            this.label3.Text = "Network Caching";
            // 
            // txtNetworkCaching
            // 
            this.txtNetworkCaching.Location = new System.Drawing.Point(244, 24);
            this.txtNetworkCaching.MaxLength = 4;
            this.txtNetworkCaching.Name = "txtNetworkCaching";
            this.txtNetworkCaching.Size = new System.Drawing.Size(37, 22);
            this.txtNetworkCaching.TabIndex = 3;
            this.txtNetworkCaching.Text = "300";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1006, 721);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Andar Remote";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabStatus.ResumeLayout(false);
            this.tabStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabRemoteControl.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabRemoteControl;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel video;
        private System.Windows.Forms.TabPage tabStatus;
        private System.Windows.Forms.TextBox hostTxt;
        private System.Windows.Forms.Button ConnectBtn;
        private System.Windows.Forms.Button StartVideoBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox ConsoleTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button pingBtn;
        private System.Windows.Forms.CheckBox audioEnabledChk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNetworkCaching;
    }
}

