﻿using System;
using System.Windows.Forms;
using AndarRobot;
using Declarations;
using Declarations.Media;
using Declarations.Players;
using Implementation;
using Infrastructure.Logging;

namespace Remote.net
{
    public partial class MainForm : Form, ITextConsoleLog
    {
        private readonly ILog log;
        private readonly Andar robot;

        public MainForm()
        {
            InitializeComponent();
            log = Log.Instance;
            log.TextConsole = this;

            log.Debug("Starting Remote App...");

            robot = new Andar(log);
        }

        public IVideoPlayer VideoStreamingPlayer { get; private set; }

        public void WriteLine(string text)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action<string>(WriteLine), text);
                    return;
                }
                ConsoleTxt.AppendText(text);
            }
            catch
            {
            }
        }

        private void ConnectStreamingBtn_Click(object sender, EventArgs e)
        {
            var button = (Button) sender;

            if (VideoStreamingPlayer == null)
            {
                var networkCaching = int.Parse(txtNetworkCaching.Text);

                log.Verbose("Starting video streaming...");
                IMediaPlayerFactory factory = new MediaPlayerFactory();
                var media = factory.CreateMedia<IMedia>(string.Format(@"rtsp://{0}:5658", hostTxt.Text));
                media.AddOptions(new[] {string.Format(":network-caching={0}", networkCaching)});
                VideoStreamingPlayer = factory.CreatePlayer<IVideoPlayer>();
                VideoStreamingPlayer.WindowHandle = video.Handle;
                VideoStreamingPlayer.Open(media);
                VideoStreamingPlayer.Events.MediaEnded += VideoStreamingPlayer_MediaEnded;
                VideoStreamingPlayer.Play();
                button.Text = "Stop Video";
                log.Debug("Video streaming started.");
            }
            else
            {
                log.Verbose("Stopping video streaming...");
                CloseVideoStreamingPlayer();
                log.Debug("Video streaming stopped.");
            }
        }

        private void CloseVideoStreamingPlayer()
        {
            try
            {
                VideoStreamingPlayer.Stop();
                VideoStreamingPlayer.Dispose();
            }
            catch (Exception)
            {
                log.Error("ERROR closing VideoStreamPlayer.");
            }

            VideoStreamingPlayer = null;

            //Update button text from UI thread
            Invoke((MethodInvoker) delegate { StartVideoBtn.Text = "Start Video"; });
        }

        private void VideoStreamingPlayer_MediaEnded(object sender, EventArgs e)
        {
            CloseVideoStreamingPlayer();
        }

        private void ConnectBtn_Click(object sender, EventArgs e)
        {
            ConnectBtn.Text = robot.ConnectToRobot(hostTxt.Text) ? "Disconnect" : "Connect";
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (robot != null)
            {
                robot.Dispose();
            }

            if (VideoStreamingPlayer != null)
            {
                VideoStreamingPlayer.Stop();
                VideoStreamingPlayer.Dispose();
            }
        }

        private void hostTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ConnectBtn_Click(this, new EventArgs());
            }
        }

        private void pingBtn_Click(object sender, EventArgs e)
        {
            if (!robot.IsConnected)
            {
                log.Warning("Not Connected to Brain.");
                return;
            }

            log.Verbose("Sending PING message...");
            robot.SendPingMessage();

            log.Debug("PING message sent.");
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (!robot.IsConnected) return;

            switch (e.KeyCode)
            {
                case Keys.W:
                    robot.Move(100, 100, 100);
                    break;
                case Keys.S:
                    robot.Move(100, -100, -100);
                    break;
                case Keys.A:
                    robot.Move(100, -100, 100);
                    break;
                case Keys.D:
                    robot.Move(100, 100, -100);
                    break;
                case Keys.Space:
                    robot.Move(100, 0, 0);
                    break;
            }
        }
    }
}