﻿using System;
using Infrastructure.Communication.Messaging;

namespace AndarRobot.Messages
{
    public class MotorsMessage : Message
    {
        public MotorsMessage(byte acceleration, short leftSpeed, short rightSpeed) : base(MessageType.Publish)
        {
            if (acceleration > 100)
            {
                throw new ArgumentException("Acceleration must be between 0 and 100");
            }

            if (leftSpeed < -100 || leftSpeed > 100)
            {
                throw new ArgumentException("leftSpeed must be between -100 and 100");
            }

            if (rightSpeed < -100 || rightSpeed > 100)
            {
                throw new ArgumentException("rightSpeed must be between -100 and 100");
            }

            Acceleration = acceleration;
            LeftSpeed = leftSpeed;
            RightSpeed = rightSpeed;
        }

        public byte Acceleration { get; set; }
        public short LeftSpeed { get; set; }
        public short RightSpeed { get; set; }

        public override byte[] Bytes
        {
            get
            {
                //Clear the payload just in case they are reusing the message
                ResetPayload();

                //Add the CustomMessageType
                PushByte((byte) CustomMessageTypes.Motors);

                //Add the fields of the Motors message
                PushByte(Acceleration);
                PushInt(LeftSpeed);
                PushInt(RightSpeed);

                return base.Bytes;
            }
        }
    }
}