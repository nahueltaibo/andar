﻿using System;
using AndarRobot.Messages;
using Infrastructure.Communication;
using Infrastructure.Communication.Messaging;
using Infrastructure.Logging;

namespace AndarRobot
{
    public class Andar
    {
        private readonly ILog log;

        public Andar(ILog log)
        {
            this.log = log;
        }

        public TcpConnection TcpConnection { get; set; }

        public bool IsConnected
        {
            get { return TcpConnection != null; }
        }

        public bool ConnectToRobot(string host, int port = 5657)
        {
            if (TcpConnection == null)
            {
                TcpConnection = new TcpConnection(host, 5657);
                TcpConnection.Connect();
                
                return true;
            }

            try
            {
                TcpConnection.Dispose();
            }
            catch (Exception ex)
            {
                log.Error(string.Format("Error disconnecting from Brain " + ex.Message));
            }
            TcpConnection = null;
            return false;
        }

        public void SendPingMessage()
        {
            var ping = new Message(MessageType.Ping);
            for (byte i = 0; i < Message.MaxPayloadLength; i++)
            {
                ping.PushByte(i);
            }
            ping.Send(TcpConnection.Stream);
        }

        public void Move(byte acceleration, short leftSpeed, short rightSpeed)
        {
            var motorsMessage = new MotorsMessage(acceleration, leftSpeed, rightSpeed);
            motorsMessage.Send(TcpConnection.Stream);
        }

        public void Dispose()
        {
            if (TcpConnection != null)
            {
                TcpConnection.Dispose();
            }
        }
    }
}