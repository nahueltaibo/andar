﻿using AndarRobot.Messages;
using Infrastructure.Communication.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AndarRobotTests
{
    [TestClass]
    public class MotorsMessageTests
    {
        [TestMethod]
        public void CreateMotorsMessageGeneratesTheCorrectMessageStructure()
        {
            var motorsMessage = new MotorsMessage(100, 100, 100);

            var bytes = motorsMessage.Bytes;
            
            Assert.AreEqual((byte)MessageType.Publish, bytes[0]);
            //6 = CustomMessageType (1) + Acceleration (1) + leftSpeed (2) + rightSpeed (2)
            Assert.AreEqual(6, bytes[1]);

            //Check CustomMessageType
            Assert.AreEqual((byte)CustomMessageTypes.Motors, bytes[2]);
            
            //Check acceleration
            Assert.AreEqual((byte)100, bytes[3]);

            //Check leftSpeed
            Assert.AreEqual((byte)100, bytes[4]);
            Assert.AreEqual((byte)0, bytes[5]);

            //Check rightSpeed
            Assert.AreEqual((byte)100, bytes[6]);
            Assert.AreEqual((byte)0, bytes[7]);


            //Change the properties values and retest
            motorsMessage.Acceleration = 50;
            motorsMessage.LeftSpeed = -100;
            motorsMessage.RightSpeed = -100;

            bytes = motorsMessage.Bytes;

            Assert.AreEqual((byte)MessageType.Publish, bytes[0]);
            //6 = CustomMessageType (1) + Acceleration (1) + leftSpeed (2) + rightSpeed (2)
            Assert.AreEqual(6, bytes[1]);

            //Check CustomMessageType
            Assert.AreEqual((byte)CustomMessageTypes.Motors, bytes[2]);

            //Check acceleration
            Assert.AreEqual((byte)50, bytes[3]);

            //Check leftSpeed
            Assert.AreEqual((byte)156, bytes[4]);
            Assert.AreEqual((byte)255, bytes[5]);

            //Check rightSpeed
            Assert.AreEqual((byte)156, bytes[6]);
            Assert.AreEqual((byte)255, bytes[7]);
        }
    }
}