package com.nt.infrastructure.messages;

/**
 * Created by Nahuel on 2015-09-07.
 */
//These are the Message Types supported by Andar (this will be the first byte on the payload
//It is not the same as Message.Type, since that is the message type of Message protocol
//The Message.Type of the message that use this Enum will always be PUBLISH
public enum MessageTypes {
    MOTORS
}
