package com.nt.infrastructure;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Nahuel on 2015-09-07.
 */
public interface Message {
    //Message types
    public static final short NO_MESSAGE = 0;
    public static final short PING = 1;
    public static final short PONG = 2;
    public static final short PUBLISH = 3;
    public static final short DISCONNECT = 4;
    public static final short HEALTH_SEQUENCE = 5;

    //For some reason, sending the message fails if it is more than 64 bytes in total (so 62 of payload)
    public static final short MAX_MESSAGE_LENGTH = 64;
    public static final short HEADER_LENGTH = 2;
    public static final short MAX_PAYLOAD_LENGTH = MAX_MESSAGE_LENGTH  - HEADER_LENGTH ;

    //Arduino Data type sizes
    public static final short ARDUINO_BYTE_SIZE = 1;
    public static final short ARDUINO_INT_SIZE = 2;
    public static final short ARDUINO_CHAR_SIZE = 1;
    public static final short ARDUINO_INT_MIN = -32768;
    public static final short ARDUINO_INT_MAX = 32767;

    short getType();
    short getPayloadLength();
    short getTotalLength();

    boolean pushByte(int arduinoByte);
    boolean pushInt(short arduinoInt);
    boolean pushChar(char arduinoChar);

    short popByte();
    short popInt();
    char popChar();

    /**
     * Returns the byte[] array representing this message
     **/
    byte[] getBytes();

    /**
     * Resets the message Payload , so Payload is 0 and Pos is 0
     **/
    void resetPayload();

    void send(OutputStream out) throws IOException;
}
