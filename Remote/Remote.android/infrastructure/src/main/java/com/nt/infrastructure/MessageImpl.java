package com.nt.infrastructure;

import android.util.Log;

import com.nt.infrastructure.Message;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Nahuel on 2015-08-23.
 */
public class MessageImpl implements Message {
    private static final String TAG = "Message";

    //Message properties
    public short type = NO_MESSAGE;
    public short payloadLength = 0;
    public byte[] payload = new byte[MAX_PAYLOAD_LENGTH];
    private short pos = 0;


    public MessageImpl(short type) {
        //Do some sanity check, to avoid malformed messages
        if (type > 255) {
            throw new IllegalArgumentException("Type argument should be smaller than 256");
        }

        this.type = type;
    }

    ///Creates a message from a raw buffer
    public MessageImpl(byte[] buffer) {
        //I don't do sanity check here, since Arduino is using unsigned bytes for the messages, so
        // there should be no way it sends bigger values.
        type = buffer[0];
        payloadLength = buffer[1];
        payload = new byte[payloadLength];

        for (int i = 0; i < payloadLength; i++) {
            payload[i] = buffer[i + 2];
        }
    }

    /**
     * Reads from the DataInputStream the contents of the Message
     **/
    public MessageImpl(InputStream inputStream) throws IOException {
        short rcv;

        //Wait and read the "type" byte
        Log.d(TAG, "Waiting for type byte...");
        rcv = (short) inputStream.read();

        //Check that the InputStream is still available
        if (rcv < 0) {
            throw new IOException("InputStream closed.");
        }
        this.type = rcv;

        //Wait and read the "payloadLength" byte
        Log.d(TAG, "Waiting for payloadLength byte...");
        rcv = (short) inputStream.read();

        //Check that the InputStream is still available
        if (rcv < 0) {
            throw new IOException("InputStream closed.");
        }
        this.payloadLength = rcv;

        //Create the payload container
        payload = new byte[this.payloadLength];

        //Wait and read the "payload" bytes
        Log.d(TAG, String.format("Waiting for %d payload bytes...", payloadLength));
        for (int i = 0; i < payloadLength; i++) {
            rcv = (short) inputStream.read();

            //Check that the InputStream is still available
            if (rcv < 0) {
                throw new IOException("InputStream closed.");
            }
            this.payload[i] = (byte) rcv;
        }

        Log.d(TAG, String.format("Message received: %s", this.toString()));
    }

    public String toString() {
        StringBuffer result = new StringBuffer("|");
        byte[] msgBytes = this.getBytes();

        for (int i = 0; i < msgBytes.length; i++) {
            result.append(String.format("%d|", msgBytes[i]));
        }

        return result.toString();
    }

    @Override
    public short getType() {
        return this.type;
    }

    @Override
    public short getPayloadLength() {
        return this.payloadLength;
    }

    @Override
    public boolean pushByte(int arduinoByte) {
        //Log.d(TAG, String.format("pushByte: %d", arduinoByte));

        if (arduinoByte < 0 || arduinoByte > 255) {
            throw new IllegalArgumentException("arduinoByte argument should be between 0 and 255");
        }

        if (enoughRoom(ARDUINO_BYTE_SIZE)) {
            payload[payloadLength++] = (byte) arduinoByte;
            return true;
        }
        return false;
    }

    @Override
    public boolean pushInt(short arduinoInt) {
        //Log.d(TAG, String.format("pushInt: %d", arduinoInt));

        if (enoughRoom(ARDUINO_INT_SIZE)) {

            payload[payloadLength++] = (byte) (arduinoInt & 0xff);
            payload[payloadLength++] = (byte) ((arduinoInt >> 8) & 0xff);

            return true;
        }
        return false;
    }

    @Override
    public boolean pushChar(char arduinoChar) {
        //Log.d(TAG, String.format("pushChar: %d", arduinoChar));

        if (arduinoChar < 0 || arduinoChar > 255) {
            throw new IllegalArgumentException("arduinoChar argument should be between 0 and 255");
        }

        if (enoughRoom(ARDUINO_CHAR_SIZE)) {
            payload[payloadLength++] = (byte) arduinoChar;
            return true;
        }
        return false;
    }

    @Override
    public short popByte() {
        byte result = payload[pos++];

        //Log.d(TAG, String.format("popByte: %d", result));

        return result;
    }

    @Override
    public short popInt() {

        byte high = payload[pos++];
        byte low = payload[pos++];

        short result = (short) ((((short) high) << 8) | low);

        //Log.d(TAG, String.format("popInt: %d", result));

        return result;
    }

    @Override
    public char popChar() {
        char result = (char) payload[pos++];

        //Log.d(TAG, String.format("popChar: %c", result));

        return result;
    }

    @Override
    public short getTotalLength() {
        return (short) (payloadLength + HEADER_LENGTH);
    }

    @Override
    public byte[] getBytes() {
        byte[] buffer = new byte[payloadLength + HEADER_LENGTH];

        buffer[0] = (byte) type;
        buffer[1] = (byte) payloadLength;

        for (int i = 0; i < payloadLength; i++) {
            buffer[i + 2] = payload[i];
        }

        return buffer;
    }

    @Override
    public void resetPayload() {
        this.payloadLength = 0;
        this.pos = 0;
        for (int i = 0; i < payload.length; i++) {
            payload[i] = 0x00;
        }
    }

    @Override
    public void send(OutputStream out) throws IOException {
        if (out != null) {
            //Get the message bytes
            byte[] buffer = this.getBytes();

            //Write it to the requested Outputstream
            out.write(buffer);
        }
    }

    public boolean enoughRoom(short requestedSize) {
        int remaining = MAX_MESSAGE_LENGTH - payloadLength;

        boolean result = remaining >= requestedSize;

        if (!result) {
            Log.e(TAG, String.format("Not enought space in message for param. Requested: %d, Remaining: %d", requestedSize, remaining));
        }

        return result;
    }
}
