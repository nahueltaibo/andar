﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Solid.Arduino;
using Solid.Arduino.Firmata;
using Andar.Infrastructure.Extensions;

namespace SolidArduinoTests
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Starting Tests task.");
            var t = Task.Run(() => RunTests());

            Console.WriteLine("Main method complete. Press <enter> to finish.");
            Console.ReadLine();
            Console.WriteLine("Waiting for Task...");
            t.Wait();
        }

        private static void RunTests()
        {
            try
            {
                var connection = new SerialConnection();
                var session = new ArduinoSession(connection, 500);
                // Cast to interface done, just for the sake of this demo.
                var firmata = session;
                var firm = firmata.GetFirmware(10);
                Console.WriteLine(@"Firmware: {0} {1}.{2}", firm.Name, firm.MajorVersion, firm.MinorVersion);

                var version = firmata.GetProtocolVersion(10);
                Console.WriteLine("Protocol version: {0}.{1}", version.Major, version.Minor);

                var caps = firmata.GetBoardCapability(10);
                Console.WriteLine("Board Capabilities:");

                foreach (var pincap in caps.PinCapabilities)
                {
                    Console.WriteLine(
                        "Pin {0}: Input: {1}, Output: {2}, Analog: {3}, Analog-Res: {4}, PWM: {5}, PWM-Res: {6}, Servo: {7}, Servo-Res: {8}",
                        pincap.PinNumber,
                        pincap.DigitalInput,
                        pincap.DigitalOutput,
                        pincap.Analog,
                        pincap.AnalogResolution,
                        pincap.Pwm,
                        pincap.PwmResolution,
                        pincap.Servo,
                        pincap.ServoResolution);
                }

                Console.WriteLine("Configuring event listeners...");
                firmata.MessageReceived += OnMessageReceived;

                Console.WriteLine(@"------------");
                Console.WriteLine(@"Reseting board");
                firmata.ResetBoard();

                Console.WriteLine(@"Trying to get the firmware version again...");
                firm = firmata.GetFirmware(5);
                Console.WriteLine(@"Firmware: {0} {1}.{2}", firm.Name, firm.MajorVersion, firm.MinorVersion);

                Console.WriteLine();

                caps = firmata.GetBoardCapability(5);
                Console.WriteLine("Board Capabilities:");

                foreach (var pincap in caps.PinCapabilities)
                {
                    Console.WriteLine(
                        "Pin {0}: Input: {1}, Output: {2}, Analog: {3}, Analog-Res: {4}, PWM: {5}, PWM-Res: {6}, Servo: {7}, Servo-Res: {8}",
                        pincap.PinNumber,
                        pincap.DigitalInput,
                        pincap.DigitalOutput,
                        pincap.Analog,
                        pincap.AnalogResolution,
                        pincap.Pwm,
                        pincap.PwmResolution,
                        pincap.Servo,
                        pincap.ServoResolution);
                }

                Console.WriteLine("Configuring sonar..");
                firmata.ConfigureSonar(2, 2, 33, 200);

                Console.WriteLine();

                caps = firmata.GetBoardCapability(5);
                Console.WriteLine("Board Capabilities:");

                foreach (var pincap in caps.PinCapabilities)
                {
                    Console.WriteLine(
                        "Pin {0}: Input: {1}, Output: {2}, Analog: {3}, Analog-Res: {4}, PWM: {5}, PWM-Res: {6}, Servo: {7}, Servo-Res: {8}",
                        pincap.PinNumber,
                        pincap.DigitalInput,
                        pincap.DigitalOutput,
                        pincap.Analog,
                        pincap.AnalogResolution,
                        pincap.Pwm,
                        pincap.PwmResolution,
                        pincap.Servo,
                        pincap.ServoResolution);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("EXCEPTION: " + ex.Message);
            }
        }

        private static void OnMessageReceived(object sender, FirmataMessageEventArgs eventargs)
        {
            switch (eventargs.Value.Type)
            {
                case MessageType.FirmwareResponse:
                    break;
                case MessageType.CapabilityResponse:
                    break;
                case MessageType.SonarResponse:
                    Console.WriteLine(eventargs.Value.Type + " " + eventargs.Value.Value);
                    break;
                case MessageType.StringData:
                    Console.WriteLine(eventargs.Value.Type + " " + ((StringData)eventargs.Value.Value).Text);
                    break;
                default:
                    Console.WriteLine(eventargs.Value.Type + " " + eventargs.Value.Value);
                    break;
            }
        }
    }
}