#include "L298NMotors.h"


L298NMotors::L298NMotors(int enA, int enB, int in1, int in2, int in3, int in4)
{
	// motor one
	enableA = enA;
	input1 = in1;
	input2 = in2;
	
	// motor two
	enableB = enB;
	input3 = in3;
	input4 = in4;

	// set all the motor control pins to outputs
	pinMode(enableA, OUTPUT);
	pinMode(enableB, OUTPUT);
	pinMode(input1, OUTPUT);
	pinMode(input2, OUTPUT);
	pinMode(input3, OUTPUT);
	pinMode(input4, OUTPUT);
}


L298NMotors::~L298NMotors()
{
	//TODO: stop the motors

}

void L298NMotors::forward(int speed){

	// turn on motor A
	digitalWrite(input1, HIGH);
	digitalWrite(input2, LOW);

	// set speed to 200 out of possible range 0~255
	analogWrite(enableA, speed);

	// turn on motor B
	digitalWrite(input3, HIGH);
	digitalWrite(input4, LOW);

	// set speed to 200 out of possible range 0~255
	analogWrite(enableB, speed);
}

