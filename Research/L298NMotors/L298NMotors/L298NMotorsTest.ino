/*
 Name:		L298NMotors.ino
 Created:	9/5/2015 11:34:16 AM
 Author:	Nahuel
*/

#include "L298nMotors.h"

// connect motor controller pins to Arduino digital pins
// motor one
int enA = 10;
int in1 = 9;
int in2 = 8;

// motor two
int enB = 5;
int in3 = 7;
int in4 = 6;

L298NMotors motors(enA, enB, in1, in2, in3, in4);

void setup()
{
}

void demoOne()
{
	// this function will run the motors in both directions at a fixed speed

	motors.forward(200);
	delay(2000);

	// now change motor directions
	digitalWrite(in1, LOW);
	digitalWrite(in2, HIGH);
	digitalWrite(in3, LOW);
	digitalWrite(in4, HIGH);
	delay(2000);

	// now turn off motors
	digitalWrite(in1, LOW);
	digitalWrite(in2, LOW);
	digitalWrite(in3, LOW);
	digitalWrite(in4, LOW);
}
void demoTwo()
{
	// this function will run the motors across the range of possible speeds
	// note that maximum speed is determined by the motor itself and the operating voltage
	// the PWM values sent by analogWrite() are fractions of the maximum speed possible 
	// by your hardware
	// turn on motors
	digitalWrite(in1, LOW);
	digitalWrite(in2, HIGH);
	digitalWrite(in3, LOW);
	digitalWrite(in4, HIGH);
	// accelerate from zero to maximum speed
	for (int i = 0; i < 256; i++)
	{
		analogWrite(enA, i);
		analogWrite(enB, i);
		delay(20);
	}
	// decelerate from maximum speed to zero
	for (int i = 255; i >= 0; --i)
	{
		analogWrite(enA, i);
		analogWrite(enB, i);
		delay(20);
	}
	// now turn off motors
	digitalWrite(in1, LOW);
	digitalWrite(in2, LOW);
	digitalWrite(in3, LOW);
	digitalWrite(in4, LOW);
}
void loop()
{
	demoOne();
	delay(1000);
	demoTwo();
	delay(1000);
}
