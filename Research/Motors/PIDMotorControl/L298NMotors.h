#ifndef L298nMotors_h
#define L298nMotors_h

#include "Arduino.h"

// Maximum speed forward
#define MAX_VALUE 255

// Maximum speed backward
#define MIN_VALUE -255

// Stop speed
#define STOP 0

class L298NMotors
{
public:
	L298NMotors(int enA, int enB, int in1, int in2, int in3, int in4);

	~L298NMotors();

	void updateMotors(int rSpeed, int lSpeed);
  void rMotorUpdate(int rSpeed);
  void lMotorUpdate(int lSpeed);
	void stop();

private:
	// connect motor controller pins to Arduino digital pins
	//enableA and enableB needs to be PWM to control motor speeds

	// motor one
	int enableA; //enableA needs to be a PWM pin
	int input1;
	int input2;

	// motor two
	int enableB; //enableB needs to be a PWM pin
	int input3;
	int input4;

	void printSpeeds(int left, int right);
};

#endif
