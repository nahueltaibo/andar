#include "L298NMotors.h"

L298NMotors::L298NMotors(int enA, int enB, int in1, int in2, int in3, int in4)
{
	// motor one
	enableA = enA;
	input1 = in1;
	input2 = in2;
	
	// motor two
	enableB = enB;
	input3 = in3;
	input4 = in4;

	// set all the motor control pins to outputs
	pinMode(enableA, OUTPUT);
	pinMode(enableB, OUTPUT);
	pinMode(input1, OUTPUT);
	pinMode(input2, OUTPUT);
	pinMode(input3, OUTPUT);
	pinMode(input4, OUTPUT);
}


L298NMotors::~L298NMotors()
{
	//TODO: stop the motors
	stop();
}

void L298NMotors::printSpeeds(int left, int right)
{
	Serial.print("left: ");
	Serial.print(left);
	Serial.print(" right: ");
	Serial.println(right);
}

void L298NMotors::updateMotors(int rValue, int lValue)
{
  rMotorUpdate(rValue);
  lMotorUpdate(lValue);
}

void L298NMotors::rMotorUpdate(int rValue)
{
  //Check ranges
  if(rValue < MIN_VALUE) { rValue = MIN_VALUE; }
  if(rValue > MAX_VALUE) { rValue = MAX_VALUE; }

  // turn on RIGHT motor
  if (rValue >= STOP)
  {
    digitalWrite(input3, HIGH);
    digitalWrite(input4, LOW);
    analogWrite(enableB, rValue);
  }
  else
  {
    digitalWrite(input3, LOW);
    digitalWrite(input4, HIGH);
    analogWrite(enableB, -rValue);
  }
}

void L298NMotors::lMotorUpdate(int lValue)
{
  //Check ranges
  if(lValue < MIN_VALUE) { lValue = MIN_VALUE; }
  if(lValue > MAX_VALUE) { lValue = MAX_VALUE; }

  // turn on RIGHT motor
  if (lValue >= STOP)
  {
    digitalWrite(input3, HIGH);
    digitalWrite(input4, LOW);
    analogWrite(enableB, lValue);
  }
  else
  {
    digitalWrite(input3, LOW);
    digitalWrite(input4, HIGH);
    analogWrite(enableB, -lValue);
  }
}

void L298NMotors::stop()
{
	digitalWrite(input1, HIGH);
	digitalWrite(input2, HIGH);
	analogWrite(enableA, STOP);

	digitalWrite(input3, HIGH);
	digitalWrite(input4, HIGH);
	analogWrite(enableB, STOP);
}

