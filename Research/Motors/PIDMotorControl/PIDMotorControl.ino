/********************************************************
 * The current project allows you to control a diferential motor vehicle
 * (tank like)using Arduino PID library.
 * This was created as research project needed for robot Andar (http://letsmakerobots.com/robot/project/andar)
 * Author: Nahuel Taibo
 * Email: nahueltaibo@gmail.com
 ********************************************************/

#include <PID_v1.h>
#include "L298NMotors.h"

// Encoder pins
#define PIN_R_ENCODER 3   // The wheel enconder attached to RIGHT motor
#define PIN_L_ENCODER 2   //The wheel enconder attached to LEFT motor

//Motors pins
#define PIN_ENB 5         // PWM pin for RIGHT motor
#define PIN_IN4 6
#define PIN_IN3 7
#define PIN_IN2 8
#define PIN_IN1 9
#define PIN_ENA 10        // PWM pin for LEFT motor

#define DEFAULT_KP 1
#define DEFAULT_KI 5
#define DEFAULT_KD 0.9
#define SAMPLE_TIME 100
// The minimum value we want on the motors output
#define MIN_OUTPUT -255
// The maximum value we want on the motors output
#define MAX_OUTPUT 255

long lastUpdateMillis = 0;    // We need to track the last update so we can update rInput every sampleTime millis

// Define Variables we need to control the RIGHT motor
volatile long rMotorTics = 0; // Will store the ticks made by the wheel encoder at RIGHT motor
long rPreviousTicks = 0;      // To calculate rInput we need to know the total tics of the previous sampleTime
double rSetpoint = 0;         // Tics per sampleTime that we want on RIGHT motor
double rInput = 0;            // Tics per sampleTime that the RIGHT motor really has.
double rOutput = 0;           // Output that needs to be writen to the RIGH motor

// Define Variables we need to control the LEFT motor
//volatile long lMotorTics = 0;  //Will store the ticks made by the wheel encoder at LEFT motor
//long lPreviousTicks = 0;      // To calculate lInput we need to know the total tics of the previous sampleTime
//double lSetpoint = 0;         // Tics per sampleTime that we want on LEFT motor
//double lInput = 0;            // Tics per sampleTime that the LEFT motor really has.
//double lOutput = 0;           // Output that needs to be writen to the LEFT motor

// Specify the links and initial tuning parameters for RIGHT motor
PID rPID(&rInput, &rOutput, &rSetpoint, DEFAULT_KP, DEFAULT_KI, DEFAULT_KD, DIRECT);

// Specify the links and initial tuning parameters for LEFT motor
//PID lPID(&lInput, &lOutput, &lSetpoint, DEFAULT_KP, DEFAULT_KI, DEFAULT_KD, DIRECT);

// Mortos object that
L298NMotors motors(PIN_ENA, PIN_ENB, PIN_IN1, PIN_IN2, PIN_IN3, PIN_IN4);

void setup()
{
  Serial.begin(115200);

  // Setting encoder pins as input
  pinMode(PIN_R_ENCODER, INPUT);
//  pinMode(PIN_L_ENCODER, INPUT);

  // Internal pullup for encoders
  digitalWrite(PIN_R_ENCODER, HIGH);
//  digitalWrite(PIN_L_ENCODER, HIGH);

  // Configure the interrupts for the encoders of both motors
  attachInterrupt(digitalPinToInterrupt(PIN_R_ENCODER), rEncoderInterrupt, FALLING);
//  attachInterrupt(digitalPinToInterrupt(PIN_L_ENCODER), lEncoderInterrupt, FALLING);

  // Initialize RIGHT motor variables
  rInput = 0;
  rSetpoint = 0;

  // Initialize LEFT motor variables
//  lInput = 0;
//  lSetpoint = 0;

  // Set the sample time on both PID controlers
  rPID.SetSampleTime(SAMPLE_TIME);
//  lPID.SetSampleTime(SAMPLE_TIME);

  // Set the output limits on oth PID controlers
  rPID.SetOutputLimits(MIN_OUTPUT, MAX_OUTPUT);
//  lPID.SetOutputLimits(MIN_OUTPUT, MAX_OUTPUT);

  //turn the PID controlers on
  rPID.SetMode(AUTOMATIC);
//  lPID.SetMode(AUTOMATIC);
}

void loop()
{
  getParam();

  // The rMotorTics and lMotorTics variables are updated by interrupts,
  // so here we calculate the real tics that happend since the last sampleTime
  rInput = rMotorTics - rPreviousTicks;
//  lInput = lMotorTics - lPreviousTicks;

  if (rPID.Compute()) { // Then we have a new value in rOutput
    //Set rPreviousTicks to the actual tics so rInput counts only the tics from this sampleTime
    rPreviousTicks = rMotorTics;

    //Update RIGHT motor output
    motors.rMotorUpdate(rOutput);

    Serial.print(rMotorTics);
    Serial.print("|");
    Serial.print(rSetpoint);
    Serial.print("|");
    Serial.print(rInput);
    Serial.print("|");
    Serial.print(rOutput);
    Serial.print("|");
//    Serial.print("|");
//    Serial.print(lMotorTics);
//    Serial.print("|");
//    Serial.print(lInput);
//    Serial.print("|");
//    Serial.print(lOutput);
    Serial.println();

}

//  if (lPID.Compute()) { // Then we have a new value in rOutput
//    //Set lPreviousTicks to the actual tics so lInput counts only the tics from this sampleTime
//    lPreviousTicks = lMotorTics;
      // Update LEFT motor output
//    motors.lMotorUpdate(lOutput);
//
//  }
}

//Used by the interrupt, adds a tic to rMotorTics
void rEncoderInterrupt()
{
  rMotorTics++;
}

//Used by the interrupt, adds a tic to lMotorTics
//void lEncoderInterrupt()
//{
//  lMotorTics++;
//}

int getParam()  
{
  char param, cmd;
  if (!Serial.available())    return 0;
  delay(10);
  param = Serial.read();                              // get parameter byte
  Serial.flush();

  int valueJump = 1;
  switch (param) {
    case 'w':                                         // adjust speed
      rSetpoint += valueJump;
//      lSetpoint += valueJump;
      break;
    case 's':                                         // adjust speed
      rSetpoint -= valueJump;
//      lSetpoint -= valueJump;
      break;
    case 'a':                                        // adjust direction
      rSetpoint += valueJump;
//      lSetpoint -= valueJump;
      break;
    case 'd':                                        // user should type "oo"
      rSetpoint -= valueJump;
//      lSetpoint += valueJump;
      break;
    case 'q':                                        // user should type "oo"
      rSetpoint = 0;
//      lSetpoint = 0;
      break;
    default:
      Serial.println("???");
  }
}

