﻿using System;
using System.IO.Ports;
using System.Windows.Forms;

namespace SonarChart.net
{
    public partial class Form1 : Form
    {
        private const int NumSensors = 12;
        private readonly int[] xValues = new int[NumSensors];
        private readonly int[] yValues = {200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200};
        private string buffer = string.Empty;
        private string divider = "|";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbSerialPorts.DataSource = SerialPort.GetPortNames();

            // Populate series data
            sonarChart.Series["Default"].Points.DataBindXY(xValues, yValues);
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            var portName = cbSerialPorts.SelectedItem.ToString();
            if (string.IsNullOrWhiteSpace(portName))
            {
                WriteLine("Invalid serial Port.");
                return;
            }

            serialPort.PortName = portName;
            serialPort.Open();
            if (!serialPort.IsOpen)
            {
                WriteLine(string.Format("Unable to open {0}", portName));
            }
        }

        public void WriteLine(String message)
        {
            try
            {
                // Check whether the caller must call an invoke method when making method calls to listBoxCCNetOutput because the caller is 
                // on a different thread than the one the listBoxCCNetOutput control was created on.
                if (console.InvokeRequired)
                {
                    UpdateConsoleDelegate update = WriteLine;
                    console.Invoke(update, message);
                }
                else
                {
                    console.Items.Add(message);
                    if (console.Items.Count > Program.MaxConsoleLines)
                    {
                        console.Items.RemoveAt(0); // remove first line
                    }
                    // Make sure the last item is made visible
                    console.SelectedIndex = console.Items.Count - 1;
                    console.ClearSelected();
                }
            }
            catch (Exception)
            {
            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            serialPort.Close();
        }

        private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //Get the string received
                var sp = (SerialPort) sender;
                buffer += sp.ReadExisting();
                if (buffer.Contains(divider))
                {
                    var indata = buffer.Substring(0, buffer.IndexOf(divider));
                    buffer = buffer.Substring(buffer.IndexOf(divider) + divider.Length);
                    // Log it to the Console
                    WriteLine(string.Format("<- {0}", indata));

                    //Assign it to the chart view
                    var readings = indata.Split(',');
                    for (var i = 0; i < readings.Length && i < yValues.Length; i++)
                    {
                        int reading;
                        int.TryParse(readings[i], out reading);
                        yValues[i] = reading;
                    }

                    UpdateSonarChartDelegate updateSonarChart = OnUpdateSonarChart;
                    console.Invoke(updateSonarChart);
                }
            }
            catch (Exception)
            {
                serialPort.Close();
            }
        }

        private void OnUpdateSonarChart()
        {
            // Populate series data
            sonarChart.Series["Default"].Points.DataBindXY(xValues, yValues);
        }

        private void serialPort_ErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            var sp = (SerialPort) sender;
            sp.Close();
        }

        private delegate void UpdateConsoleDelegate(String msg);

        private delegate void UpdateSonarChartDelegate();
    }
}