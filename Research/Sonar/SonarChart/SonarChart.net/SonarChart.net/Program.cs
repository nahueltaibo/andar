﻿using System;
using System.Windows.Forms;

namespace SonarChart.net
{
    internal static class Program
    {
        public const int MaxConsoleLines = 1000;

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}