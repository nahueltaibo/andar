#include "Message.h"


Message::Message(byte theType)
{
	type = theType;

	//Clean the payloads buffer;
	for (int i = 0; i < sizeof(payload); i++){
		payload[i] = 0;
	}
}


Message::~Message()
{
}

byte Message::getType()
{
	return type;
}

byte Message::getLength()
{
	return length;
}


Message Message::processPing()
{
	Message pong(PONG);

	while(getLength()>0)
	{
		pong.push(popByte());
	}
}

void Message::debug(bool incoming){
#ifdef DEBUG
	switch (type)
	{
		case PING: Serial.print("PING"); break;
		case PONG: Serial.print("PONG"); break;
		case PUBLISH: Serial.print("PUBLISH"); break;
		case DISCONNECT: Serial.print("DISCONNECT"); break;
		default: Serial.print("UNKNOWN"); break;
	}

	if (incoming)
	{
		Serial.print(" <- ");
	}
	else
	{
		Serial.print(" -> ");
	}

	Serial.print("|");
	Serial.print(type);
	Serial.print("|");
	Serial.print(length);
	Serial.print("|");

	for (int i = 0; i < length; i++)
	{
		Serial.print(payload[i]);
		Serial.print("|");
	}
	Serial.println();
#endif
}

bool Message::push(byte byteVal)
{
  Serial.print("pushByte: ");
  Serial.println(byteVal);

  if (enoughRoom(sizeof(byteVal)))
	{
		payload[length++] = byteVal;
		return true;
	}
	return false;
}

bool Message::push(int intVal)
{
  Serial.print("pushInt: ");
  Serial.println(intVal);
	if (enoughRoom(sizeof(intVal)))
	{
		//Now write the low byte
		payload[length++] = lowByte(intVal);

		//First write the high byte
		payload[length++] = highByte(intVal);

		return true;
	}
	return false;
}

bool Message::push(char charVal)
{
  Serial.print("pushChar: ");
  Serial.println(charVal);
	if (enoughRoom(sizeof(charVal)))
	{
		payload[length] = charVal;
		length += sizeof(charVal);
		return true;
	}
	return false;
}

byte Message::popByte()
{
	//as it is just a byte, it is quite straight forward
	byte result = payload[pos++];

	Serial.print("popByte: ");
	Serial.println(result);

	return result;
}

int Message::popInt()
{
	//The payload should have: |HIGH|LOW|
	//As we read from right to left, we first red the low byte, and then the high byte
	byte high = payload[pos++];
	byte low = payload[pos++];
	
	int result = word(low, high);

	Serial.print("popInt: ");
	Serial.println(result);

	return result;
}

char Message::popChar()
{
	//as a char has the same size of a byte, it is quite straight forward
	char result = payload[pos++];

	Serial.print("popChar: ");
	Serial.println(result);

	return result;
}

bool Message::enoughRoom(int requestedSize)
{
	int remaining = MAX_MESSAGE_LENGTH - length;

	bool result = remaining >= requestedSize;

#ifdef DEBUG
  if(!result){
    Serial.print("Error: Not enought space in message for param. Requested: ");
    Serial.print(requestedSize);
    Serial.print(" Remaining: ");
    Serial.println(remaining);
  }
#endif

  return result;
}
