﻿using System;
using Andar.Brain.Model.Schemas;

namespace Andar.Brain.Model.Behaviors
{
    public abstract class BaseBehavior<T> : IBehavior<T>
    {
        public IMotorSchema MotorSchema { get; set; }
        public ISensorSchema<T> SensorSchema { get; set; }

        public virtual void Run()
        {
            if (MotorSchema == null) throw new ArgumentNullException("Cant run without a MotorSchema!");
            if (SensorSchema == null) throw new ArgumentNullException("Cant run without a SensorSchema!");
        }
    }
}