﻿using Andar.Brain.Model.Schemas;

namespace Andar.Brain.Model.Behaviors
{
    public interface IBehavior<T>
    {
        ISensorSchema<T> SensorSchema { get; set; }
        IMotorSchema MotorSchema { get; set; }
        void Run();
    }
}