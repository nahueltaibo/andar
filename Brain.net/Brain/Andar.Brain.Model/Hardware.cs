﻿using Solid.Arduino;

namespace Andar.Brain.Model
{
    public struct Hardware
    {
        /// <summary>
        ///     Sketch's Name (Returned by firmata.GetFirmware().Name)
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        ///     Connection stablished with the device (if any)
        /// </summary>
        public EnhancedSerialConnection Connection { get; set; }

        /// <summary>
        ///     Arduino Session stablished with the device (if any)
        /// </summary>
        public ArduinoSession Session { get; set; }
    }
}