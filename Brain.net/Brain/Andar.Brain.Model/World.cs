﻿using System.Collections.Generic;
using Andar.Brain.Model.Schemas;

namespace Andar.Brain.Model
{
    public class World
    {
        public IList<IRangeFinder> Sonar { get; set; }
    }
}