﻿using System;

namespace Andar.Brain.Model.Schemas
{
    /// <summary>
    ///     Sensor Schema that represents a sensor
    /// </summary>
    /// <typeparam name="T">The Reading type the sensor retrieves</typeparam>
    public interface ISensorSchema<T> : ISchema
    {
        /// <summary>
        ///     Last reading taken by the sensor
        /// </summary>
        T LastReading { get; }

        /// <summary>
        ///     Called when a readin is taken
        /// </summary>
        event EventHandler ReadingTaken;

        void GotReading(T reading);
    }
}