﻿using System;
using Solid.Arduino.Sonar;

namespace Andar.Brain.Model.Schemas
{
    public class HcSr04RangeFinder : IRangeFinder
    {
        public int MaxDistance => 200;
        public int TriggerPin { get; set; }
        public int EchoPin { get; set; }
        public int PingInterval { get; set; }
        public SonarReading LastReading { get; set; }
        public event EventHandler ReadingTaken;

        public void GotReading(SonarReading reading)
        {
            LastReading = reading;
            OnReadingTaken();
        }

        protected virtual void OnReadingTaken()
        {
            ReadingTaken?.Invoke(this, EventArgs.Empty);
        }
    }
}