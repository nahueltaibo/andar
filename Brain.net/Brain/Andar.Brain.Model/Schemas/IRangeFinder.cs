﻿using Solid.Arduino.Sonar;

namespace Andar.Brain.Model.Schemas
{
    public interface IRangeFinder : ISensorSchema<SonarReading>
    {
        /// <summary>
        ///     The pin that the sonar uses to trigger readings
        /// </summary>
        int TriggerPin { get; set; }

        /// <summary>
        ///     The pin that the sonar uses to receive a response
        /// </summary>
        int EchoPin { get; set; }

        /// <summary>
        ///     Frequency with wich the sonar is triggered
        /// </summary>
        int PingInterval { get; set; }

        /// <summary>
        ///     Maximum distance the fisical device is able to detect
        /// </summary>
        int MaxDistance { get; }
    }
}