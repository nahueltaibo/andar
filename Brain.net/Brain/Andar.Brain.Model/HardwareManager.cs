﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Andar.Brain.Model.Exceptions;
using Andar.Brain.Model.Schemas;
using Andar.Infrastructure.Logging;
using Solid.Arduino;
using Solid.Arduino.Firmata;
using Solid.Arduino.Sonar;

namespace Andar.Brain.Model
{
    /// <summary>
    ///     Responsible for receivig the updates from hardware and updating the sensors accordingly
    /// </summary>
    public class HardwareManager
    {
        private static HardwareManager _instance;
        private readonly ILog log;
        private Hardware backbone = new Hardware {Name = "Backbone"};
        private Hardware neck = new Hardware {Name = "Neck"};
        public List<IRangeFinder> Sonar;

        private HardwareManager()
        {
            log = Logger.Instance;

            var task1 = new Task(ConfigureHardware);

            task1.Start();
        }

        /// <summary>
        ///     Returns the only instance of the Sensor Manager
        /// </summary>
        public static HardwareManager Instance => _instance ?? (_instance = new HardwareManager());

        public event EventHandler HardwareConfigured;

        private void ConfigureHardware()
        {
            ConfigureConnections();

            Thread.Sleep(1000);
            ConfigureSonar();

            OnHardwareConfigured();
        }

        private void ConfigureSonar()
        {
            log.Debug("Configuring Sonar...");
            Sonar = new List<IRangeFinder>();
        
            var rangeFinder = new HcSr04RangeFinder {TriggerPin = 2, EchoPin = 2, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 3, EchoPin = 3, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 4, EchoPin = 4, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 5, EchoPin = 5, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 6, EchoPin = 6, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 7, EchoPin = 7, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);
            rangeFinder = new HcSr04RangeFinder {TriggerPin = 8, EchoPin = 8, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 9, EchoPin = 9, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 10, EchoPin = 10, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 11, EchoPin = 11, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 12, EchoPin = 12, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);

            rangeFinder = new HcSr04RangeFinder {TriggerPin = 13, EchoPin = 13, PingInterval = 33};
            Sonar.Add(rangeFinder);
            neck.Session.ConfigureSonar(rangeFinder.TriggerPin, rangeFinder.EchoPin, rangeFinder.PingInterval,
                rangeFinder.MaxDistance);
            Thread.Sleep(100);
        }

        public void ConfigureConnections()
        {
            var ports = SerialPort.GetPortNames();

            foreach (var port in ports)
            {
                var connection = new EnhancedSerialConnection(port, SerialBaudRate.Bps_115200);
                var session = new ArduinoSession(connection, 500);

                for (var i = 0; i < 10; i++)
                {
                    try
                    {
                        // Cast to interface done, just for the sake of this demo.
                        IFirmataProtocol firmata = session;

                        var firm = firmata.GetFirmware();

                        if (firm.Name == neck.Name)
                        {
                            ConfigureNeck(port, firm, firmata, connection, session);

                            //If we where able to configure the port, we dont need to keep trying
                            break;
                        }

                        if (firm.Name == backbone.Name)
                        {
                            ConfigureBackbone(port, firm, firmata, connection, session);

                            //If we where able to configure the port, we dont need to keep trying
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Warn("The port {0} did not answerd correctly to firmata protocol: {1}", port, ex.Message);
                        //Wait a while to re try
                        Thread.Sleep(3000);
                    }
                }
            }

            if (neck.Session == null)
            {
                throw new HardwareException("Unable to connect with Neck!");
            }

            //if (backbone.Session == null)
            //{
            //    throw new HardwareException("Unable to connect with Backbone!");
            //}
        }

        private void ConfigureBackbone(string port, Firmware firm, IFirmataProtocol firmata,
            EnhancedSerialConnection connection,
            ArduinoSession session)
        {
            log.Info("Backbone connected on port {0}", port);
            log.Info("\tSketch: {0} {1}.{2}", firm.Name, firm.MajorVersion, firm.MinorVersion);

            var version = firmata.GetProtocolVersion();
            log.Info("\tFirmata: {0}.{1}", version.Major, version.Minor);

            backbone.Connection = connection;
            backbone.Connection.ErrorReceived += OnBackboneError;

            backbone.Session = session;

            //If the Arduino was already on, we need to clear its configuration, so we can re map everything
            backbone.Session.ResetBoard();
            Thread.Sleep(100);
        }

        private void ConfigureNeck(string port, Firmware firm, IFirmataProtocol firmata,
            EnhancedSerialConnection connection,
            ArduinoSession session)
        {
            log.Info("Neck connected on port {0}", port);
            log.Info("\tSketch: {0} {1}.{2}", firm.Name, firm.MajorVersion, firm.MinorVersion);

            var version = firmata.GetProtocolVersion();
            log.Info("\tFirmata: {0}.{1}", version.Major, version.Minor);

            neck.Connection = connection;
            neck.Connection.ErrorReceived += OnNeckError;

            neck.Session = session;
            neck.Session.MessageReceived += OnNeckMessageReceived;

            //If the Arduino was already on, we need to clear its configuration, so we can re map everything
            //neck.Session.ResetBoard();
            Thread.Sleep(1000);
        }

        private void OnNeckMessageReceived(object sender, FirmataMessageEventArgs eventargs)
        {
            switch (eventargs.Value.Type)
            {
                case MessageType.StringData:
                    log.Info(((StringData) eventargs.Value.Value).Text);
                    break;
                case MessageType.SonarResponse:
                    var reading = (SonarReading) eventargs.Value.Value;
                    IRangeFinder rangefinder;

                    if (Sonar != null &&
                        (rangefinder = Sonar.FirstOrDefault(rf => rf.TriggerPin == reading.Pin)) != null)
                    {
                        //If the event is fired by an old configuration of a sonar, ignore this event, until the new configuration is in place
                        rangefinder.GotReading(reading);
                        //log.Info("Pin: {0}, Distance: {1}", reading.Pin, reading.Distance);
                    }
                    break;
                default:
                    log.Debug("Neck> Message: Type: {0}, Value: {1}", eventargs.Value.Type, eventargs.Value.Value);
                    break;
            }
        }

        private void OnBackboneError(object sender, SerialErrorReceivedEventArgs e)
        {
            log.Error("Backbone> {0}", e.ToString());
        }

        private void OnNeckError(object sender, SerialErrorReceivedEventArgs e)
        {
            log.Error("Neck> {0}", e.ToString());
        }

        protected virtual void OnHardwareConfigured()
        {
            log.Debug("Hardware configured.");
            HardwareConfigured?.Invoke(this, EventArgs.Empty);
        }
    }
}