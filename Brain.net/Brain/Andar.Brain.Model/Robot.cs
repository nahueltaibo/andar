﻿using System;
using Andar.Infrastructure.Logging;

namespace Andar.Brain.Model
{
    /// <summary>
    ///     This is the main entry point for Andar Robot
    /// </summary>
    public class Robot
    {
        private static Robot _robot;
        private readonly ILog log;
        public HardwareManager HardwareManager;
        // There will be only one robot
        private Robot()
        {
            log = Logger.Instance;
        }

        public static Robot Instance => (_robot ?? (_robot = new Robot()));
        public event EventHandler RobotStarted;

        public void Start()
        {
            try
            {
                HardwareManager = HardwareManager.Instance;
                HardwareManager.HardwareConfigured += OnHardwareConfigured;
            }
            catch (Exception ex)
            {
                log.Error("Robot: {0}", ex.Message);
            }
        }

        private void OnHardwareConfigured(object sender, EventArgs e)
        {
            RobotStarted?.Invoke(this, EventArgs.Empty);
        }
    }
}