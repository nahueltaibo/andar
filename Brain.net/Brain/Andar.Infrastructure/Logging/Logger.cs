﻿using log4net;
using log4net.Config;

namespace Andar.Infrastructure.Logging
{
    public class Logger : ILog
    {
        private static ILog _logger;
        private readonly log4net.ILog baseLogger;

        private Logger()
        {
            baseLogger = LogManager.GetLogger(typeof (Logger));
            BasicConfigurator.Configure();
        }

        public IUiLogger UiLogger { get; set; }

        public static ILog Instance
        {
            get { return _logger ?? (_logger = new Logger()); }
        }

        public void Fatal(string message, params object[] args)
        {
            UiLogger?.Log(LogType.Fatal, message, args);

            baseLogger.FatalFormat(message, args);
        }

        public void Error(string message, params object[] args)
        {
            UiLogger?.Log(LogType.Error, message, args);

            baseLogger.ErrorFormat(message, args);
        }

        public void Warn(string message, params object[] args)
        {
            UiLogger?.Log(LogType.Warn, message, args);

            baseLogger.WarnFormat(message, args);
        }

        public void Info(string message, params object[] args)
        {
            UiLogger?.Log(LogType.Info, message, args);

            baseLogger.InfoFormat(message, args);
        }

        public void Debug(string message, params object[] args)
        {
            UiLogger?.Log(LogType.Debug, message, args);

            baseLogger.DebugFormat(message, args);
        }
    }
}