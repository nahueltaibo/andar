﻿namespace Andar.Infrastructure.Logging
{
    public interface ILog
    {
        void Fatal(string message, params object[] args);
        void Error(string message, params object[] args);
        void Warn(string message, params object[] args);
        void Info(string message, params object[] args);
        void Debug(string message, params object[] args);
        IUiLogger UiLogger { set; }
    }
}