namespace Andar.Infrastructure.Logging
{
    public interface IUiLogger
    {
        void Log(LogType type, string message, object[] args);
    }
}