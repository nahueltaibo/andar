﻿namespace Andar.Infrastructure.Logging
{
    public enum LogType
    {
        Fatal,
        Error,
        Warn,
        Info,
        Debug
    }
}