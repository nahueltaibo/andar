﻿using System;
using Andar.Infrastructure.Logging;
using Solid.Arduino.Firmata;

namespace Andar.Infrastructure.Extensions
{
    public static class FirmataExtensions
    {
        public static Firmware GetFirmware(this IFirmataProtocol firmata, int retries)
        {
            var i = 0;
            while (true)
            {
                try
                {
                    return firmata.GetFirmware();
                }
                catch (TimeoutException)
                { 
                    i++;
                    Logger.Instance.Warn("{0} try failed.", i);
                    if (i >= retries)
                        throw;
                }
            }
        }
        public static ProtocolVersion GetProtocolVersion(this IFirmataProtocol firmata, int retries)
        {
            var i = 0;
            while (true)
            {
                try
                {
                    return firmata.GetProtocolVersion();
                }
                catch (TimeoutException)
                {
                    i++;
                    Logger.Instance.Warn("{0} try failed.", i);
                    if (i >= retries)
                        throw;
                }
            }
        }

        public static BoardCapability GetBoardCapability(this IFirmataProtocol firmata, int retries)
        {
            var i = 0;
            while (true)
            {
                try
                {
                    return firmata.GetBoardCapability();
                }
                catch (TimeoutException)
                {
                    i++;
                    Logger.Instance.Warn("{0} try failed.", i);
                    if (i >= retries)
                        throw;
                }
            }
        }
        

    }
}