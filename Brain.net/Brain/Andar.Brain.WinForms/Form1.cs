﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Andar.Brain.Model;
using Andar.Brain.Model.Schemas;
using Andar.Infrastructure.Logging;

namespace Andar.Brain.WinForms
{
    public partial class Form1 : Form, IUiLogger
    {
        private LogType consoleErrorLevel = LogType.Debug;
        private ILog log;
        private Robot robot;
        private bool scrollConsole = true;

        public Form1()
        {
            InitializeComponent();
        }

        public void Log(LogType type, string message, object[] args)
        {
            if (type <= consoleErrorLevel)
            {
                try
                {
                    uiConsole.BeginInvoke(
                        (Action)
                            (() =>
                                uiConsole.Items.Add(new KeyValuePair<LogType, string>(type, string.Format(message, args)))));
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            log = Logger.Instance;
            log.UiLogger = this;

            log.Debug("Logger started");

            robot = Robot.Instance;
            robot.RobotStarted += OnRobotStarted;
            robot.Start();
        }

        private void OnRobotStarted(object sender, EventArgs e)
        {
            for (var i = 0; i < 12; i++)
            {
                robot.HardwareManager.Sonar[i].ReadingTaken += UpdateSonar;
            }
        }

        private void UpdateSonar(object sender, EventArgs e)
        {
            var sensor = (IRangeFinder) sender;
            var index = robot.HardwareManager.Sonar.FindIndex(s => s == sensor);
            try
            {
                SonarChart.BeginInvoke((Action) (() =>
                {
                    SonarChart.Series["Radar"].Points[index].YValues[0] = sensor.LastReading.Distance;
                    SonarChart.Refresh();
                }));

                PolarPlotChart.BeginInvoke((Action) (() =>
                {
                    PolarPlotChart.Series["PolarPlot"].Points[index].YValues[0] = sensor.LastReading.Distance;
                    PolarPlotChart.Refresh();
                }));
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void uiConsole_DrawItem(object sender, DrawItemEventArgs e)
        {
            var g = e.Graphics;
            var logEntry = (KeyValuePair<LogType, string>) uiConsole.Items[e.Index];


            Brush foregroundBrush;
            var textFont = e.Font;
            switch (logEntry.Key)
            {
                case LogType.Fatal:
                    Font = new Font(e.Font, FontStyle.Bold);
                    foregroundBrush = new SolidBrush(Color.Red);
                    break;
                case LogType.Error:
                    foregroundBrush = new SolidBrush(Color.Red);
                    break;
                case LogType.Warn:
                    foregroundBrush = new SolidBrush(Color.DarkOrange);
                    break;
                case LogType.Info:
                    foregroundBrush = new SolidBrush(Color.Black);
                    break;
                case LogType.Debug:
                default:
                    foregroundBrush = new SolidBrush(Color.DarkGray);
                    break;
            }
            var rectangle = new RectangleF(new PointF(e.Bounds.X, e.Bounds.Y),
                new SizeF(e.Bounds.Width, g.MeasureString(logEntry.Value, textFont).Height));
            g.DrawString(logEntry.Value, textFont, foregroundBrush, rectangle);
            foregroundBrush.Dispose();
            g.Dispose();

            if (scrollConsole) uiConsole.TopIndex = uiConsole.Items.Count - 1;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            scrollConsole = !scrollConsole;
            uiConsole.Refresh();
        }

        private void fatalLevelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consoleErrorLevel = LogType.Fatal;
        }

        private void errorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consoleErrorLevel = LogType.Error;
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consoleErrorLevel = LogType.Info;
        }

        private void warnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consoleErrorLevel = LogType.Warn;
        }

        private void debugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consoleErrorLevel = LogType.Debug;
        }
    }
}